% !TEX root = ../../thesis.tex

\section{Reactive and Incremental Language}
\label{sec:reactive_lang}

We start with a reactive and evolvable framework~\cite{Mateus:Tese} for web and cloud applications.
It provides operations for dealing with data and how it can be displayed, as well as how the application behaves.
Due to the reactive nature of the language~\cite{Miguel:Seco:Paper}, every change made to the state of the application is propagated through a graph of dependencies, giving the developer immediate feedback and keeping the state synchronized.
Each change made to an application is statically verified to guarantee that the application evolves safely.
These changes are automatically integrated with the running application without disrupting the availability of the application.
The combination of the reactive and incremental aspects allow the developer to build applications in a live environment, which greatly increases productivity in the development process.

There are three core operations in the language: \textbf{var}, \textbf{def}, and \textbf{do}.
The purpose of the \textbf{var} operation is to declare names that store the application's persistent state.
The \textbf{def} operation declares named pure data transformations.
Finally, the \textbf{do} operation performs actions that change the state, and it is through this operation that the user interacts with an application.
Actions are introduced with the keyword \textbf{action}.

The reactive nature begins with the \textbf{def} operation, which builds a graph of dependencies to propagate changes made to the state, keeping it always up-to-date.
Pure data transformations (\textbf{def}) are only modified through the propagation of changes, and variables (\textbf{var}) through actions.

\begin{figure}[htbp]
\centering
\begin{minipage}{0.75\textwidth}
\centering
\lstinputlisting[frame=bt,float=false]{Chapters/Figures/square_calc_example}
\end{minipage}%
\hfill
\begin{minipage}{0.25\textwidth}
\centering
\includegraphics[height=1in]{dependencies}
\end{minipage}
\caption{Square calculator example and the associated data dependencies graph}
\label{fig:square_calc_example}
\end{figure}

To help further understand how to use the language, \cref{fig:square_calc_example} illustrates how a simple square number calculator can be implemented.
This example defines four names: \code{x}, \code{setX}, \code{square}, and \code{calc}.
\code{x} is a state variable initialized with the number \code{2}, and it will store the number that we wish to square.
\code{square} is a pure data transformation storing the result of the square of \code{x}.
\code{setX} creates an action for modifying the state stored by \code{x}.
Since \code{square} depends on \code{x}, changing \code{x} updates the value of \code{square}, thus reflecting the desired calculation of the square of a given number.
\code{calc} defines an \gls{html} document in which we display the current value stored in \code{x}, and the current value of the square of \code{x} stored in \code{square}.
To provide a new number we create an input in the document with a specific \code{id}, \code{newValue}, which we can then use to execute the \code{setX} action with a button.
The action is given the argument \code{\#newValue} which refers to the input with the corresponding \emph{id} attribute.
Because the document is also a pure data transformation, it will be updated every time that the state changes in one of the dependencies.

\subsection{Runtime Support System}
\label{ssec:runtime_support_system}

The framework is supported by a runtime system~\cite{Mateus:Tese} with three main components:
\begin{itemize}[noitemsep]
\item \textbf{Interpreter} -- parses, verifies, evaluates, and executes code;
\item \textbf{Database} -- stores application data;
\item \textbf{Web Server} -- provides a \gls{rest} \gls{api} for the interpreter, and pushes updates via WebSockets~\cite{WebSocket:Online}.
The three most important routes of the \gls{rest} \gls{api} are described in \cref{table:rest}.
\end{itemize}
\begin{table}[]
\centering
\caption{REST API}
\label{table:rest}
\begin{tabular}{r|l}
\hline
\multicolumn{1}{c|}{Description} & \multicolumn{1}{c}{HTTP  Request}
\\ \hline
%
Get the value of a name & GET /:\textbf{name}/:\textbf{args}
%
\\
\myVSpace[20pt]
Execute code &
\begin{tabular}[t]{@{}l@{}}
POST /
\\
data: \{
\\\MyIndent exp: \textbf{code}\\
\}
\end{tabular}
\\
%
\myVSpace[20pt]
Do actions &
\begin{tabular}[t]{@{}l@{}}
PUT /
\\
data: \{
\\\MyIndent action: \textbf{action},
\\\MyIndent args: \{
\\\MyIndent\MyIndent \textbf{arg}: \{
\\\MyIndent\MyIndent\MyIndent type: \textbf{type},
\\\MyIndent\MyIndent\MyIndent value: \textbf{value}
\\\MyIndent\MyIndent\}
\\\MyIndent\},
\\\MyIndent env: \{
\\\MyIndent\MyIndent name: \textbf{value}
\\\MyIndent\}
\\\}
\end{tabular}
\end{tabular}
\end{table}

\Cref{fig:runtime} illustrates the interactions between each component.
\begin{figure}[tbp]
\includegraphics[width=\textwidth]{runtime}
\caption{Runtime Support System architecture~\cite{Mateus:Tese}}
\label{fig:runtime}
\end{figure}
A typical cycle of interactions with between a client and the system starts with the client subscribing to a workspace through WebSockets.
When a client subscribes, the existing workspace data is pushed to the client.
The client then performs requests to the server \gls{rest} interface to execute code in the interpreter, storing any new or updated data in the database.
All changes in the database are pushed back to all subscribed clients through the WebSockets.
