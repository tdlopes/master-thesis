% !TEX root = ../../thesis.tex

\section{Approach}

To tackle the problems that were previously discussed, this thesis seeks to introduce language abstractions that help design and evolve cloud and web applications that require data to be shared in, and between, different isolated groups of users.

The development of authentication mechanisms require client identification.
The most common approach to client identification in modern frameworks is a server generated unique identifier stored on the client side.
We adopt the same approach and introduce a server generated \gls{usid} to enable the runtime system to distinguish clients.
In the context of the programming language, this identifier is exposed through the reserved, string typed, keyword \textbf{usid}.
The introduced \gls{usid} allows the developer to build authentication

Many languages like C~\cite{C:Modules}, and OCaml~\cite{OCaml:Modules} employ a modular approach as a means of isolating code to manage the complexity of programs, or to hide information.
Keeping in mind the modular approach of these languages, we introduce our \textbf{module} abstraction as isolated environments where data is shared with all the users.
To control user access to modules we introduce guard conditions that can be defined with modules.

Modules are isolated environments, thus the sate outside of a module is not accessible from inside the module.
Most languages provide an import operation for their modular approach, typically granting access to an exposed interface of \emph{getters} and \emph{setters} to manage encapsulated data.
Thus, we introduce an \textbf{import} operation with which the developer can create views in a module over states defined in an outside environment.
Import operations are supported by \textbf{lenses}~\cite{Lenses:Pierce:Paper,Relation:Lenses:Proceeding,Steckermeier:Lenses}, which provide bidirectional transformations over a concrete state: a \emph{get} transformation that given a concrete state produces a view, and a \emph{put} transformation updates a concrete state with an updated view.
We introduce different kinds of lenses to cover a wide range of filters for an imported module state.

\glspl{mta} need to provide a different state of the same application to each tenant, so that a tenant can manipulate its own state and not another tenant's state.
With this in mind, we expand the introduced module abstraction with parameterization, allowing modules to be defined with a set of parameters.
The parameters act as an index for each defined module state, thus transforming the module into an indexed module.
Defined parameters can be explicitly set as non-indexing parameters.
An indexed module provides views of each module state, with each view being accessed through the module indexing parameters (index).
To provide different views of the same state we apply the lens concept over a map structure where each entry is a concrete state, and the lens \emph{moves} to an entry with a given index to apply the lens transformations.
The composition of module parameterization and module access conditions allow for the definition of strong access control by defining conditions that depend on indexing parameters.

So far, the introduced abstractions provide tools to create authentication mechanisms and applications that can quickly be transformed to provide an array of different application views and states.
However, role based development is not yet covered, which is a very important trait of the \glspl{mta}.
User roles follow a strict hierarchy of access, and are commonly implemented in modern frameworks by explicitly defining roles and creating rules associated with each role.
In our approach, we allow modules to be defined inside other modules as nested modules.
Nested modules create a strict hierarchy of access by inheriting the ancestors conditions and isolating the nested module state from them.
Typically, after the definition of user roles in modern frameworks, the framework provides a set of operations that helps determine at runtime if a user has a certain role or not.
To this end, we introduce a similar runtime conditional access check operation, that closely resembles an \emph{If-Then-Else} statement, called \emph{In-Then-Else}.
This operation checks if a given user has access to a given module at runtime and depending on the result, either the branch \emph{then} or the branch \emph{else} is evaluated.
Combining nested modules, access conditions, \emph{In-Then-Else}, and import operations provides a powerful tool in the development of role based applications, without explicit long hard-written queries for role definition and access control.

Often, in a modular approach there is a need to access an already defined module to avoid writing the same code multiple times.
Commonly languages with a modular approach provide some kind of inheritance or extension mechanism to deal with such needs.
In our approach, we introduce module inheritance to also tackle this problem.
Considering that our modular approach introduces module parameterization, to inherit a module we have to consider that the module can be a parameterized module.
With that in mind, to inherit a module the heir must provide arguments to the inherited module parameters.
When a module inherits another module, the heir gains full access to the inherited module state.
To guarantee that access to the inherited module state is checked when accessing an inherited state, the module access condition is also inherited.

Our approach is language based, with most of the introduced abstractions mapping directly to core language constructs, thus keeping the incremental and reactive properties of the core language.
Additionally, our approach needs only a few modifications to the type system keeping the guarantee of a sound development process, correct code, and that no errors occur when the application is deployed.

The combination of the introduced abstractions gives the developer the ability to develop complex reactive cloud and web applications in a live environment, where each modification made to the application can be used right away.
This improves the quality of the development process with faster feedback and faster development, which would otherwise be slower and harder in current web frameworks.
To help the development process, we provide a web-based live programming environment to give immediate and continuous feedback to the developer and to simplify development with the module abstraction by providing module navigation and inspection tools.
