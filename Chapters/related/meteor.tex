% !TEX root = ../thesis.tex

\subsection{Meteor}

Meteor~\cite{Meteor:Online} is a \gls{js} Web Framework written in Node.js~\cite{NodeJS} which allows for rapid prototyping and cross-platform code production.
One of the key features of Meteor is its ability to automatically propagate data changes to clients without any additional code from the developer.
The Synchronization process is achieved with the \gls{ddp}~\cite{MeteorDDP:2016:Online} and a publish-subscribe pattern~\cite{XEP0060:Online}.
By default, the publish-subscribe pattern automatically publishes everything (\textit{auto-publish} package, which should only be used for fast prototyping), meaning that the entire database is present on the client without any filtering.

Another very important feature of Meteor is the use of collections.
In Meteor, data is stored in synchronized collections that are available both on the server and on the client.
Although there are several projects to support more database systems, MongoDB~\cite{MongoDB} is currently the most stable and maintained.
On the server, the collection is stored on a MongoDB database by default, and on the client the collections are managed through a \gls{js} implementation of MongoDB in memory~\cite{Meteor:2016:Online}.
This gives the client access to a collection without talking to the server.
To keep certain fields of the collection values from being transferred to the client, the publish-subscribe pattern can be customized for each collection accordingly.
Finally, when creating a Meteor collection, a connection can be fed to the constructor which specifies a server to manage that collection, making Meteor collections very powerful in multi-server applications.

To show how an application is built in Meteor, consider the simple example application in \cref{lst:meteor_example_html,lst:meteor_example_js}, which a client can post messages and view a wall of all of the posted messages, live, in the same page.
\begin{lstlisting}[language=html,caption={Meteor application - HTML},label={lst:meteor_example_html}]
<body>
  <h1>Post</h1>
  <form class="new-message">
    <input type="text" name="message" placeholder="Message..." />
    <input type="text" name="author" placeholder="Author" />
  </form>
  <h1>Wall</h1>
  <ul>
    {{#each getMessages}}
      <div>Message:{{message}}</div>
      <div>Author:{{author}}</div>
    {{/each}}
  </ul>
</body>
\end{lstlisting}
\begin{lstlisting}[language=js,caption={Meteor application - JS},label={lst:meteor_example_js}]
Wall = new Mongo.Collection("wall");

if (Meteor.isClient) {
  Template.body.helpers({
    getMessages: function () {
      return Wall.find({});
    }
  });

  Template.body.events({
    "keypress .new-message": function (event) {
      if (event.which != 13) return;
      var message = event.target.parentElement.message; // get message
      var author = event.target.parentElement.author; // get author
      Wall.insert({
        message: message.value,
        author: author.value
      });
      message.value = ""; // clear message
      author.value = ""; // clear author
    }
  });
}

if (Meteor.isServer) {
  Meteor.startup(function() {});
}
\end{lstlisting}
First, we look at the \gls{js} code, then the \gls{html}.
In Meteor we can write client and server code in the same file if we want, and to distinguish client code from server code, two boolean values are available through the Meteor object: \code{Meteor.isClient} and \code{Meteor.isServer}.
Code written outside these contexts is ran on both sides.
The first thing we declare is a MongoDB collection to store the messages, which will be available both on the client and on the server as explain before.
On the client, we introduce two new things: template helpers and template events.
Template helpers provide the views on the client with functions or values, in our example we implement a function that returns every record in the \code{Wall} collection.
Template events are useful to map functions to events that might occur in the template.
We map a \code{keypress} event on an element with a class named \code{new-message} (in our example the element is a form) to a function that, in case the pressed key is an \emph{Enter}, retrieves the values (message and author) from the inputs and adds a new record to the collection, that is, a new message.
On the server side, it simply starts the server with one function and a callback.
Finally, in the view, we have a simple form with an input field for the new message, and another for the author name.
Bellow the form, we list the collection by using the template language of Meteor (Spacebars), to iterate over all the messages that the template helper \code{getMessages} returns.

\subsubsection{Sessions}

In Meteor, sessions are accessed through a global object on the client in which the user can store an arbitrary set of key-value pairs.
Sessions are also reactive, when a value for a session key is set, the change is propagated to the client and the user can see the page automatically change with the new value.
\Cref{lst:meteor_reactive_html,lst:meteor_reactive_js} defines an example of a reactive session.
\begin{lstlisting}[language=html,caption={Meteor reactive session - HTML},label={lst:meteor_reactive_html}]
<template name="main">
  <p>Session var has: {{showX}}</p>
</template>
\end{lstlisting}
\begin{lstlisting}[language=js,caption={Meteor reactive session - JS},label={lst:meteor_reactive_js}]
Template.main.helpers({
  showX: function () {
    return Session.get("x");
  }
});

Session.set("x", "1");
// Page will say "Session var says: 1"

Session.set("x", "2");
// Page will say "Session var says: 2"
\end{lstlisting}
The \code{Session} keyword is the global object that accesses the session key-value pairs, and it is where we store the variable \code{x}.
In the template \code{showX} function we fetch and return the value of \code{x} stored in the session.
This way, when the value is changed, the page with the template will update automatically.
By default, sessions in Meteor are not permanent, if a page is refreshed the session object will be a new one.
There are, however, available packages which provide implementations for persistent sessions, some with Server Side Session others Client Side Session approach.

Meteor provides a package manager~\cite{AtmosphereJS:Online} for developers to publish and distribute packaged code.
To implement user authentication, Meteor provides a set of packages to manage accounts.
The accounts base package uses a Meteor collection to store the users and keeps the session token in the local storage of the browser to make the session permanent.
Additionally, a package can be added that exposes templates to add login forms to an application that work out of the box with the base package.

\subsubsection{Access Control}

Meteor provides \emph{allow} and \emph{deny} methods for collections which give the developer the freedom of controlling every action done on a collection, be it an insert, update or remove operation. For example, \cref{lst:meteor_access_control} shows how to control who can insert in a collection of messages.
\begin{lstlisting}[language=js,caption={Meteor access control},label={lst:meteor_access_control}]
Wall = new Mongo.Collection("wall");

Wall.allow({
  insert: function (user, message) {
    return (user && message.author === user);
  }
});
\end{lstlisting}
Whenever a client executes such operations, the defined methods are called to check whether the operation is allowed to continue or not.
Although this is a very flexible way of creating access control, it requires the developer to create code for every operation of every collection if he wishes to specify what is allowed.

To simplify the steps in defining access control in Meteor, a package named \textit{meteor-roles} offers the ability to attach permissions to the already existing users collection.
With it, the developer can define the permissions for a user inside a domain.
Then, those permissions can be verified.
For example, in \cref{lst:meteor_roles} we assign the role of \emph{wall moderator} to the user Alice, to check if Alice does indeed have the \code{moderator role}, and that she does not have the \code{admin} role.
\begin{lstlisting}[language=js,caption={Meteor roles package},label={lst:meteor_roles}]
Roles.addUsersToRoles(AliceId, ['moderator'], 'wall')
Roles.userIsInRole(AliceId, 'moderator', 'wall')  // => true
Roles.userIsInRole(AliceId, 'admin', 'wall')  // => false
\end{lstlisting}
Even thou the developer still needs to address each operation with this package, it simplifies the aggregation of permissions in a large set of users.

\subsubsection{Synchronization}

Meteor synchronization is achieved with the \gls{ddp} and publish-subscribe pattern.
\gls{ddp} is a very simple protocol for fetching structured data from a server, it works like a \gls{rest} service but through web sockets, giving live updates when data changes.
In Meteor, a client communicates with the server through this protocol.
Note that the protocol works as a \gls{rest} interface, meaning that a connection can be made outside a Meteor application, both server or client side, to receive updates following the \gls{ddp}.
To control what goes where Meteor uses the publish-subscribe pattern, where the developer can specify what is published and who subscribes through a couple of methods (publish and subscribe).
