% !TEX root = ../thesis.tex

\subsection{Yesod}

Yesod is a web framework based on the Haskell language~\cite{Haskell} for developing type-safe \gls{rest} model based web applications~\cite{Yesod:2016:Online}.
Type safety is the key feature of Yesod, by giving high-level declarative techniques, the developer can define the expected input types, as well as having the guarantee that the output is also well formed through the process of type-safe \gls{url}s.
Yesod provides entity definition in a higher level, with all the necessary process of persisting and loading data being performed inside so that the developer can remain ignorant to the details.
The Yesod framework also shines performance wise.
Using the Haskell's \gls{ghc} as well as allowing \gls{html}, \gls{css} and \gls{js} to be analyzed at compile time, Yesod provides great performance by avoiding disk I/O at runtime.

Yesod makes good use of Haskell's features to save time in developing with code generation.
Code generation comes in two forms, scaffolding for starting projects faster, and libraries.
Using the libraries means that the generated code will always be up to date and everything is taken care of at compile time.
All the code can be written manually, without using library specific code generation, if more control is required.

One such library is the \gls{th}, which essentially generates an Haskell abstract syntax tree, reducing a boilerplate code in many occasions.
\gls{qq} is a minor extension to the \gls{th} library, and an important library.
It allows arbitrary content to be embedded within Haskell source files.
Consider \cref{lst:yesod_th_qq}, while a \gls{th} function like \code{hamletFile} can read the template contents from a file, \gls{qq} provides one named \code{hamlet} that reads the contents inline.
\begin{lstlisting}[caption={Yesod TH and QQ functions},label={lst:yesod_th_qq}]
-- TH function
$(hamletFile "template.hamlet")

-- QQ function
[hamlet|<p>This is quasi-quoted Hamlet.|]
\end{lstlisting}

In \cref{lst:yesod_example}, we defined an example wall application to demonstrate how a small application is built in Yesod.
\begin{lstlisting}[float=false,language=yesod,caption={Yesod Wall application example},label={lst:yesod_example}]
{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE ViewPatterns               #-}

import Control.Applicative ((<$>), (<*>))
import Data.Text (Text)
import Yesod
import Yesod.Form.Jquery
import Database.Persist.Sqlite
import Control.Monad.Trans.Resource (runResourceT)
import Control.Monad.Logger (runStderrLoggingT)

share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
Message
    author Text
    text Text
    deriving Show
|]

data Wall = Wall ConnectionPool

mkYesod "Wall" [parseRoutes|
/ WallR GET
/post PostR POST
|]

instance Yesod Wall

instance RenderMessage Wall FormMessage where
    renderMessage _ _ = defaultFormMessage

instance YesodPersist Wall where
    type YesodPersistBackend Wall = SqlBackend

    runDB action = do
        Wall pool <- getYesod
        runSqlPool action pool

messageAForm :: AForm Handler Message
messageAForm = Message
    <$> areq textField "Author" Nothing
    <*> areq textField "Message" Nothing

messageForm :: Html -> MForm Handler (FormResult Message, Widget)
messageForm = renderTable messageAForm

getWallR :: Handler Html
getWallR = do
  wall <- runDB $ selectList [] []
  (widget, enctype) <- generateFormPost messageForm
  defaultLayout [whamlet|
    <p> Post a new message!
    <form method=post action=@{PostR} enctype=#{enctype}>
      ^{widget}
      <button>Post
    <p> Messages!
      $forall Entity messageid message <- wall
        <p>Message: #{messageText message}
        <p>Author: #{messageAuthor message}
  |]

postPostR :: Handler Html
postPostR = do
  ((result, _), _) <- runFormPost messageForm
  case result of
    FormSuccess message -> do
      runDB $ insert message
      redirect $ WallR
    _ -> redirect $ WallR

openConnectionCount :: Int
openConnectionCount = 10

main :: IO ()
main = runStderrLoggingT $ withSqlitePool "demo.db3" openConnectionCount $
  pool -> liftIO $ do
    runResourceT $ flip runSqlPool pool $ do
        runMigration migrateAll
    warp 3000 $ Wall pool
\end{lstlisting}
At the top, we declared the extensions being used in the application, also known as language \emph{pragmas}, followed by the imported classes required for what we need to build.
The \code{share} function generates the code for building the entity representing the messages as well as the code required for persisting that entity (in our example we use the SQLite~\cite{SQLite} library), while the quasi-quote function \code{persistLowerCase} converts a whitespace-sensitive syntax into a list of entity definitions.
The data type declared as \code{Wall} represents the foundation data type of the application, and it must be an instance of the \code{Yesod} type class as declared afterwards.
The foundation data type of an application can store a variety of things, and in this case it stores the connection pool for persisting data.
Next, we have the route definitions.
One route for the main page, where we will show all the message and a form for posting a new message, and a route for the form action that inserts new messages.
The following defined instances included in our application are, respectively, used for automatic generation of our \code{Message} entity form, and for persisting data in the application with a function that runs an action in the connection pool.
The following two functions, \code{messageAForm} and \code{messageForm}, are used to ultimately build a table with a form for the \code{Message} entity.
The next two functions, \code{getWallR} and \code{getPostR}, respectively handle the requests for the \code{WallR} and \code{PostR} routes.
In \code{getWallR} we first fetch all the messages, then, we generate the widget for the \code{Message} entity for building our form, and finally, we create the page to be returned.
In the page we have the form for posting new messages as well as a list of all the messages, which are iterated with a \code{forall} function given the already fetched list of messages from the database.
For handling the POST requests, the \code{getPostR} checks the result of the form, if it is a success it inserts the new message in the database and redirects to the main page.
For simplicity, any other result redirects to the main page.
Finally, we declare the number of possible open connections in the pool, and we define our main function.
In it we start a logger, the connection pool for SQLite, and the Warp Webserver with our \code{Wall} foundation and connection pool.

\subsubsection{Sessions}

Unlike the previously studied frameworks, Yesod by default implements sessions in a Client Side web session style with a package named \code{clientsession}.
Data is stored in an HTTP Cookie using encryption and signatures, which overcomes the security concerns that rise from using Client Side web sessions.
Encryption ensures that the user can't inspect the Cookie and understand its contents, and signatures protect the Cookie from being tampered with.
To change the file path for the encryption key for client session or the session timeout, one can override the \code{makeSessionBackend} method in the \code{Yesod} type class.
Also, if we want to turn off session handling this method can be overridden to return \code{Nothing}, although Cross-Site Request Forgery protection is also disabled along side session handling.
There are, however, other functions for finer grain control of session configurations.

The only remaining security concern is that using the client sessions over \gls{http} brings the inherent vulnerability of an attacker being able to read the traffic and impersonating the user by obtaining his Cookie.
\gls{ssl} is the only solution to this vulnerability, and preventing browsers from accessing the site with \gls{http}.
To run the entire site over \gls{ssl}, Haskell has a solution called \textit{warp-tls}, and to prevent the site from sending Cookies over insecure connections we can apply transformations to the \code{makeSessionBackend} method.
This transformation turns on the Secure \emph{bit} of Cookies in order for the browsers not to transmit over \gls{http} to the domain.

The \gls{api} for the base session is available through four functions: \code{lookupSession} to retrieve a value (if available) with a given a key, \code{getSession} to retrieve all of the key/value pairs, \code{setSession} to set a value for a given key, and \code{deleteSession} to clear a value for a given key.
Recalling \cref{lst:yesod_example}, in \cref{lst:yesod_sessions} we set a session value with the most recent post when it is successfully inserted in the \code{PostR} route, and retrieve it when handling the \code{WallR} route.
\begin{lstlisting}[language=yesod,caption={Yesod sessions},label={lst:yesod_sessions}]
getWallR :: Handler Html
getWallR = do
  wall <- runDB $ selectList [] []
  (widget, enctype) <- generateFormPost messageForm
  value <- lookupSession "recentPost"
  defaultLayout [whamlet|
    $maybe v <- value
      <p> Session value: #{v}
    <p> Post a new message!
    <form method=post action=@{PostR} enctype=#{enctype}>
      ^{widget}
      <button>Post
    <p> Messages!
      $forall Entity messageid message <- wall
        <p>Message: #{messageText message}
        <p>Author: #{messageAuthor message}
  |]

postPostR :: Handler Html
postPostR = do
  ((result, _), _) <- runFormPost messageForm
  case result of
    FormSuccess message -> do
      runDB $ insert message
      setSession "recentPost" message
      redirect $ WallR
    _ -> redirect $ WallR
\end{lstlisting}

Yesod also provides a pair of functions to enable the storage of messages in the session for sending success and failure messages to a redirected page.
A \code{setMessage} to store a message in the session, and \code{getMessage} to read and clear the previously stored message.
In \cref{lst:yesod_messages} we define a success message and error messages to be stored when posting on the wall, which are then shown in the wall page.
\begin{lstlisting}[language=yesod,caption={Yesod session messages},label={lst:yesod_messages}]
getWallR :: Handler Html
getWallR = do
  wall <- runDB $ selectList [] []
  (widget, enctype) <- generateFormPost messageForm
  message <- getMessage
  defaultLayout [whamlet|
    $maybe m <- message
      <p> Message: #{m}
    <p> Post a new message!
    <form method=post action=@{PostR} enctype=#{enctype}>
      ^{widget}
      <button>Post
    <p> Messages!
      $forall Entity messageid message <- wall
        <p>Message: #{messageText message}
        <p>Author: #{messageAuthor message}
  |]

postPostR :: Handler Html
postPostR = do
  ((result, _), _) <- runFormPost messageForm
  case result of
    FormSuccess message -> do
      runDB $ insert message
      setMessage "Post Success"
      redirect $ WallR
    _ -> do
      setMessage "Post Failed"
      redirect $ WallR
\end{lstlisting}

\subsubsection{Access Control}

Authentication in Yesod is supported through third-party authentication systems, like OpenID~\cite{OpenID}, BrowserID~\cite{BrowserID}, and OAuth~\cite{OAuth}.
It also supports the basic and more common mechanism of username/password systems.
While the latter provides more control over the application development, adding third-party authentication systems in Yesod is simple and users don't have to remember a new set credentials.
The package providing these authentication plugins is called \emph{yesod-auth}.
For each plugin it is required that users are identified with a unique string, for example, in BrowserID an email address is used.
Despite the mechanisms behind each plugin, at the end of a successful login process the plugins set a value in the session indicating the user's \emph{AuthId}.
This is usually persisted in a table for tracking users.
Due to the use of the underlying session mechanism of Yesod, the stored authentication value is safe with the same encryption, as well as having the same timeout as the session for the period in which the user is authenticated.

To build an application using on of these plugins, a type class named \code{YesodAuth} is used to specify a number of settings as well as requiring six declarations:
\begin{itemize}
\item the \code{AuthId} representing the value that is returned when asking whether a user is logged in or not;
\item the \code{getAuthId} function for fetching the \code{AuthId} which contains the used authentication backend, the actual identifier, and a list for storing extra information;
\item a redirect route for a successful login named \code{loginDest};
\item a redirect route for a successful logout named \code{logoutDest};
\item an \code{authPlugins} list containing the plugins used in our application;
\item an \gls{http} connection manager for allowing third-party login systems to share connections reducing the cost of restarting \gls{tcp} connections with each request.
\end{itemize}
\Cref{lst:yesod_auth} sets up an authentication application, and defines a route named \code{AuthR} to support the access to the sub-site for authentication.
\begin{lstlisting}[language=yesod,caption={Yesod Authentication},label={lst:yesod_auth}]
import           Data.Default                (def)
import           Network.HTTP.Client.Conduit (Manager, newManager)
import           Yesod
import           Yesod.Auth
import           Yesod.Auth.BrowserId

-- ...

data App = App { httpManager :: Manager}

instance YesodAuth App where
  type AuthId App = Text
  getAuthId = return . Just . credsIdent
  loginDest _ = WallR
  logoutDest _ = WallR
  authPlugins _ =
    [authBrowserId def]
  authHttpManager = httpManager

mkYesod "App" [parseRoutes|
  /auth AuthR Auth getAuth
|]
\end{lstlisting}
Defining the route \code{AuthR} requires an additional two parameters, the authentication sub-site, and a function that retrieves the sub-site value which Yesod automatically provides.
If more than one plugin is used, Yesod automatically unfolds the login hyper links for each plugin and provides the route with the appropriate sub-site being requested.

Finally, we can query the user's \code{AuthId} with the \code{maybeAuthId} function.
\Cref{lst:yesod_is_logged} checks if the user is logged in when accessing the main page of the example application.
\begin{lstlisting}[language=yesod,caption={Yesod logged in status},label={lst:yesod_is_logged}]
getWallR :: Handler Html
getWallR = do
  maid <- maybeAuthId
  defaultLayout [whamlet|
      $maybe id <- maid
        <p>You are logged in as: #{show id}
        <p> <a href=@{AuthR LogoutR}>Logout
      $nothing
        <p> <a href=@{AuthR LoginR}>Go to the login page
  |]
\end{lstlisting}

Now that our application can authenticate users, we can control the access of their requests.
Yesod provides authorization in a simple and declarative manner through two methods: \code{authRoute} and \code{isAuthorized}.
These methods are added to the \code{Yesod} type class instance.
\code{authRoute} should point to the login page, which is almost always \code{AuthR LoginR}.
The function \code{isAuthorized} takes a requested route and a \code{boolean} value indicating if the request is anything but a \emph{GET}, \emph{HEAD}, \emph{OPTIONS}, or \emph{TRACE} request.
In it we can write code, such as accessing the file system or the database.
As an example, consider \cref{lst:yesod_authorization}, in which we define a route \code{AdminR} where only the user named \code{admin} is permitted.
\begin{lstlisting}[language=yesod,caption={Yesod Authorization},label={lst:yesod_authorization}]
instance Yesod App where
  authRoute _ = Just $ AuthR LoginR
  isAuthorized AdminR _ = isAdmin
  isAuthorized _ _ = return Authorized

isAdmin = do
  maid <- maybeAuthId
  return $ case maid of
    Nothing -> AuthenticationRequired
    Just "admin" -> Authorized
    Just _ -> Unauthorized "You must be an admin"
\end{lstlisting}
The \code{AuthenticationRequired} value will redirect the user to the login page as defined by \code{authRoute}, and the \code{Authorized} value will validate the user.

Although the code we have to write is quite extensive if we want more complex control over the resources, Yesod provides with a highly customizable authentication solution, with much of the boilerplate code being generated for the user.
