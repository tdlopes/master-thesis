\subsection{WebDSL}
\label{ssec:webdsl}

WebDSL~\cite{WebDSL} is a domain-specific language for developing dynamic web applications that translates to Java web applications, and has some key features that are relevant to our topic, such as a rich data model (entities), access control and generation of a synchronization framework.
WebDSL applications are organized in \emph{\_*.app\_} files.
Modules in WebDSL can be written in .app files and imported to a main .app file where the application header is declared.
To show how an application is built in WebDSL, consider \cref{lst:webdsl_example}, a very simple example application in which a client can post messages and view a wall of all of the posted messages.
\begin{lstlisting}[language=webdsl,caption={WebDSL example application}, label={lst:webdsl_example}]
application example

entity Message {
  author :: String
  text :: Text
}

entity Wall {
  posts -> List<Message>
}

var wall := Wall{}

define page root() {
  title { "Wall" }
  navigate(post()){ "Post" } " "
  for(m: Message in wall.posts) {
    "Author: " output(m.author)
    "Message: " output(m.text)
  }
}

define page post() {
  title { "Post a new message" }
  var m := Message{}
  form {
    label("Input Message:"){input(m.text)}
    label("Author:"){input(m.author)}
    submit("Post", action {
      m.save();
      wall.posts.add(m);
      message("New post created.");
      return root();
    })
  }
}
\end{lstlisting}
Following the application header, two entities are declared, an entity representing a message, and an entity representing a wall to store all the messages in a collection.
The \code{Wall} variable is instantiated followed by two pages, one iterates the collection of messages to show a wall of messages, and the other provides a form in which a client can input a text message to post.

\subsubsection{Sessions}

WebDSL follows a Server Side web session style where session data is stored on the server with globally visible variables in the application, also known as session entities~\cite{WebDSL:2016:Online}.
\Cref{lst:webdsl_session} shows an example of how to define a session entity for a wall of messages.
\begin{lstlisting}[language=webdsl,caption={WebDSL session entity},label={lst:webdsl_session}]
session wall {
  messages -> List<Message>
}
\end{lstlisting}
A session entity object is automatically instantiated when a browser makes a request to a server, which responds with a cookie named \emph{WEBDSLSESSIONID} with the identifier for that client session.
Session entities, just like regular entities, can be extended in order to add new properties to an already existing session entity.
In \cref{lst:webdsl_session_extension} we add a list of friends to our wall session entity.
\begin{lstlisting}[language=webdsl,caption={WebDSL session entity extension},label={lst:webdsl_session_extension}]
extend session wall {
  friends -> List<User>
}
\end{lstlisting}

\subsubsection{Access Control}
\label{sssec:access_control}

Access control, in WebDSL, is defined using a sub-language used to define rules over resources.
To allow the creation of rules over an authenticated principal, the sub-language supports the declaration of a principal using a user defined entity and a set of credentials, which generates a session entity to hold the currently signed in user.
\Cref{lst:webdsl_conf_security} shows a configuration of a principal using the \code{User} entity.
\begin{lstlisting}[caption={WebDSL principal definition},label={lst:webdsl_conf_security}]
entity User {
  name :: String
  password :: Secret
}
principal is User with credentials name, password
\end{lstlisting}
The generated session entity is show in \cref{lst:webdsl_security_context}.
\begin{lstlisting}[caption={WebDSL security context},label={lst:webdsl_security_context}]
session securityContext {
  principal -> User
  loggedIn :: Bool := this.principal != null
}
\end{lstlisting}
In this declaration two things are introduced, the principal representing the currently signed in user, and a function to verify whether the principal is signed in or not.
In the current implementation of WebDSL the authentication credentials are not used, but in the future, the WebDSL developers pretend to derive a default login template from the given authentication credentials.
Additionally, login and logout templates are generated along side an authenticate function that checks if the given credentials are correct, and setting the principal property if they are.
The auto-generated templates and function, can be overridden to allow further control of the authentication step.
Creating access control rules becomes very simple after the \code{securityContext} is configured, for example, in \cref{lst:webdsl_access_control} we define a rule stating that only the author of a message can edit it.
\begin{lstlisting}[caption={WebDSL access control rules},label={lst:webdsl_access_control}]
access control rules

rule page editMessage(m:Message) {
  m.author == principal
}
\end{lstlisting}
The first line states that the following declarations will be access control rules.
Rules can be applied to other resources like templates, page actions, functions or pointcuts, which will be explained later.
In this example, our resource is a page, named \code{editMessage}, with a message as an argument.
The \code{securityContext} session is available inside the rules, and by using the principal declaration we can check if the author of the message is the currently logged user.

Nested rules are allowed for a finer-grained control, but with some constraints as show in \cref{lst:webdsl_control_cw}.
\begin{lstlisting}[caption={WebDSL access control constraints},label={lst:webdsl_control_cw}]
// Compiles
rule page editMessage(m:Message) {
 m.author == principal

 rule action save() {
   m.author == principal
 }

 rule action cancel() {
   m.author == principal
 }
}

// Does not compile
rule action save() {
 m.author == principal

 rule page editMessage(u:Message) {
   m.author == principal
 }
}
\end{lstlisting}
Pages are parent resources of actions, this implies that a nested rule is only valid for usage of that resource inside the parent resource.
Often, a page resource allows all its actions with the same rule for accessing the page, therefore describing a page rule automatically makes all its actions follow the same rule.
For example, \cref{lst:webdsl_rule_unfold} shows the unfolded \code{editMessage} page rule.
\begin{lstlisting}[caption={WebDSL unfolded rules},label={lst:webdsl_rule_unfold}]
rule page editMessage(m:Message) {
  m.author == principal

  rule action *(*) {
    m.author == principal
  }
}
\end{lstlisting}
The symbol \code{*} states that any action with any number of arguments will be matched.

Resources can also be grouped into \code{pointcuts} in order to apply the same rule to a group of resources.
In \cref{lst:webdsl_pointcuts} we create rules to control user actions with \code{pointcuts}.
\begin{lstlisting}[caption={WebDSL pointcuts},label={lst:webdsl_pointcuts}]
pointcut userSection(u:User) {
 page editUser(u),
 page post(u)
}

rule pointcut userSection(u:User){
 u == principal
}
\end{lstlisting}

\subsubsection{Synchronization Framework}

WebDSL provides generation of code through the WebDSL \gls{ide} for a synchronization framework, which exposes web-services for external applications to use.
To generate the synchronization framework it is required that a WebDSL application, with at least a complete model, is already in place.
Setting up the framework requires a declaration of a \code{String} property that represents the object and enables data partitioning, which is primarily used to reduce data sent to mobile applications that access the synchronization webservices.
\Cref{lst:webdsl_sync_top} defines a synchronization configuration for the \code{Message} entity.
\begin{lstlisting}[caption={WebDSL top level synchroinzation},label={lst:webdsl_sync_top}]
entity Message {
  id :: String

  synchronization configuration {
    toplevel name property : id
  }
}
\end{lstlisting}
The configuration allows for the definition of access control rules over data to control which external sources accessing the data synchronization framework can read, write entities or create instances of those entities.
These access control rules can only be written if a principal is previously defined, which in this case should be for authenticating devices accessing the framework.
In \cref{lst:webdsl_sync_access} we define access control rules for the synchronization of the \code{Message} entity.
\begin{lstlisting}[caption={WebDSL synchronization access control},label={lst:webdsl_sync_access}]
entity Message {
  id :: String

  synchronization configuration{
    toplevel name property : id
    access read: true
    access write: Logedin()
    access create: principal.isAdmin()
  }
}
\end{lstlisting}
Lastly, it allows the configuration of restricted properties, meaning that the declared properties will not be shared in the synchronization.
\Cref{lst:webdsl_sync_restrict} defines the property \code{fullName} restriction for the \code{User} entity synchronization.
\begin{lstlisting}[caption={WebDSL synchronization property restriction},label={lst:webdsl_sync_restrict}]
entity User {
  name :: String
  fullName :: String

  synchronization configuration{
    restricted properties : fullName
  }
}
\end{lstlisting}

After the synchronization framework files are generated, they are imported to the application to make the web-services available to the applications.
The available web-services are called with \emph{POST} requests to access the core synchronization functions.
The \gls{url} is structured as follows:
\begin{center}
\emph{http://<websiteurl>/webservice/<webservicename>}
\end{center}
The services provided at the \gls{url} are:
\begin{itemize}
  \item getTopLevelEntities
  \item getTimeStamp
  \item syncNewObjects
  \item syncDirtyObjects
  \item sync
\end{itemize}
The framework also adds three functions to entities for serializing data, because the values in the database are not in the format required for transmission through the web-services.
The framework provides a page for each entity where stored data can be browsed.
