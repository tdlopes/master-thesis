% !TEX root = ../thesis.tex

\subsection{Opa Language}

The Opa Language is a full-stack web framework language for the development of web applications~\cite{OPA:2016:Online}.
It can be used for both client-side and server-side scripting with the Opa language, which is then compiled to Node.js on the server and \gls{js} on the client.
The database programming is in MongoDB.
The Opa language implements strong, static typing which helps avoid many security issues, for example, \gls{sql} injections or cross-site scripting attacks.
One of the biggest features of Opa Language is the \emph{Power Rows}, which are extended \gls{js} objects, they have the flexibility of dynamic languages but with a type checker that keeps the language safe.
Another feature, and one that we explore, is the slicer that Opa lang uses to determine where each top-level declaration runs (client side, server side, or both).

\subsubsection{Sessions}

Opa language provides three primitives for communication between clients and servers: Session, Cell and Network.
Network is used to broadcast messages for multiple clients, and Cell is a two-way, synchronous communication.
Session is a one-way asynchronous communication, and it is the one we will be focusing on.

The Opa standard library description of session says:

	``A session is a unit of state and concurrency. A session can be created on a server or on a client, and can be shared between several servers.''

Sessions in Opa, are supported by encapsulating an imperative state and using message passing to communicate with message handlers \cite{OPASessions:2011:Online}.
Upon receipt of a message by the handler, the session can be changed, or even terminated, with a set of available instructions illustrated in \cref{lst:opa_session_inst}.
\begin{lstlisting}[language=opa,caption={Opa Lang session instructions},label={lst:opa_session_inst}]
type Session.instruction(state)=
  {set: state} /** Carry on with a new value of the state value.*/
  {unchanged}   /** Carry on with the same state.*/
  {stop}        /** Stop this session. Any further message will be ignored */
\end{lstlisting}

Creating a session requires an initial state of the session and a message handler, and returns a channel to which messages can be sent to be processed by the message handler.
Messages can be sent from/to different servers/clients.
\Cref{lst:opa_session} shows an example of a session that keeps track of a friends list.
\begin{lstlisting}[language=opa,caption={Opa Lang session example},label={lst:opa_session}]
function handleFriends(oldState, m) {
  match(m) {
    case {add : user} : {
      set : List.add(user, oldstate)
    }
    default : {
      unchanged
    }
  }
}

friends = Session.make([], handleFriends)

function addFriend(user) {
  Session.send(friends, {add : user})
}
\end{lstlisting}
Here, we define a message handler to change the state when a message matches the first case by returning the \code{set} instruction, and by default keeping the state intact with the \code{unchanged} instruction.
The session is then created with an initial state, an empty list, and the handler previously declared.
The returned value is a channel which we then use to define a function that adds friends in a session.

\subsubsection{Slicer}

The Opa language can be executed on both the server and the client, and so, it must be decided, at compile time, where the code actually runs.
This decisions are made by the Opa language slicer~\cite{OPASlicer:2016:Online}.
With slicing annotations the slicer can be told where each top-level declaration should run.
There are three possible slicing annotations that can be written before the \code{function} keyword: \code{client}, \code{server}, and \code{both}.
Each one tells the slicer where to run the code, but it does not mean that it is invisible to the other side.
When running on both sides, it either executes the side effects on both sides or it only executes on the server and shares the results.

When slicing annotations are omitted, the slicer decides to place declarations on the server if possible, otherwise it places them on the only possible side.
Writing a slice annotation on a module defaults all the declarations inside it to the same annotations, but they can be overridden with other slice annotations.
There are however some rules due to the simple fact that everything can not be placed on both sides.
Primitives declared on one side can only be placed on that side.
If a primitive is sliced server only, it also means that it is \textit{server private}.
Primitives tagged as \textit{server private} cannot be called by the client, and all declarations using it will become \textit{server private} themselves.
There is however a directive (\textit{publish}) to stop this propagation of the tag, essentially telling the client can now see the declaration (e.g release data after an authentication mechanism succeeds).

Sometimes the developer wants to have different behaviors for a declaration depending on the side it is running.
To this end Opa lang provides a way of this as shown in \cref{lst:opa_sliced_state}.
\begin{lstlisting}[language=opa,caption={Opa Lang explicit sliced states},label={lst:opa_sliced_state}]
number = @sliced_expr({server: 2, client: 1})
do println(side)
\end{lstlisting}
