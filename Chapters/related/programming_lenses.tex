\section{Programming with Lenses}

Propagating changes between connected structures (e.g. databases and materialized views) is usually done in ad-hoc fashion, that is, hand-written transformations from one structure to another and back.
Naturally, when the structures involved are complex manual management and maintenance of such transformations becomes equally complex.
Writing such bidirectional transformations is a problem in a vast set of domains, including data converters and synchronizers, picklers and unpicklers (serializing data), structure editors and constraint maintainers for user interfaces~\cite{Lenses:Pierce:Paper}.

\emph{Lenses} are bidirectional transformations between a set of inputs \code{C} (``concrete structures'') and a set of outputs \code{A} (``abstract structures'').
\code{A} \emph{lens} \textbf{l} is comprised of three functions:
\begin{gather*}
	l.get \in C \rightarrow A	\\
	l.put \in A \rightarrow C \rightarrow C \\
	l.create \in A \rightarrow C
\end{gather*}
The \emph{get} function is a forward transformation, a total function from \code{C} to \code{A}.
The \emph{put} function takes an old \code{C} with an updated \code{A} and produces a correspondingly updated \code{C}.
The \emph{create} function works like the \emph{put} function, except that it only takes an \code{A} argument (if the only available structure is \code{A} then defaults are supplied).

\subsection{Boomerang}

In order to illustrate what \emph{lenses} can do, we show a practical example in Boomerang \cite{Boomerang:Pierce:Paper}, a programming language for writing lenses.
Boomerang was developed to operate on ad-hoc, textual data with a set of \emph{string lens combinators} based on familiar regular operators (union, concatenation, and Kleene-star), and to address issues in manipulation of ordered data (\emph{dictionary lenses}).

Consider that we want write a lens whose \emph{get} function takes as a concrete state newline-separated records, like the one defined in \cref{lst:boom_concrete}, with comma separated data about students, their age, and college name.
\begin{lstlisting}[language=boomerang,caption={Concrete state defined in Boomerang},label={lst:boom_concrete}]
let c : string =
  Alice Lopes, 28, FCT
  Bob Lopes, 21, UCP
  Charlie Martins, 21, FCT
\end{lstlisting}
We want the returned abstract state in \cref{lst:boom_abstract}.
\begin{lstlisting}[language=boomerang,caption={Abstract state from \emph{get} transformation in Boomerang},label={lst:boom_abstract}]
Alice Lopes, 28
Bob Lopes, 21
Charlie Martins, 21
\end{lstlisting}
First we define the three regular expressions in \cref{lst:boom_regexp} to make the writing of the lens simpler to read.
\begin{lstlisting}[language=boomerang,caption={Regular expressions in Boomerang},label={lst:boom_regexp}]
let NAME : regexp = [A-Za-z ]+
let AGE : regexp = [0-9]{2}
let COLLEGE : regexp = [A-Z]+
\end{lstlisting}
The regular expressions match, respectively, student names, their age, and the college name.
Next we define the lens.
\begin{lstlisting}[language=boomerang,caption={Lens definition in Boomerang},label={lst:boom_lens}]
let compA : lens = key NAME . ", " . AGE . del ", " . del COLLEGE
let compsA : lens =
  "" | <dictionary "":compA> . (newline . <dictionary "":compA>)*
\end{lstlisting}
The lens is broken down into two parts for easier comprehension.
The first declaration of the lens shows how each record line chunk is transformed.
The second declaration goes over each line applying the first declaration.

The lens composition \emph{get} function uses the \code{del} lens to indicate that the college is to be removed.
Note that the concatenation operator \code{``.''} as well as other operators automatically promote their arguments, following the sub-typing relationships: string <: regexp <: lens.
The \code{key} is an annotation used to indicate which part of the line chuck is used for alignment, so each line is iterated in an orderly fashion.
This is complemented using a \emph{dictionary lens} in \code{compsA}.
Without this dictionary, the alignment would be positional and we would not get the expected results when the record lines have different positional alignment.
The \emph{put} function updates the old concrete structure with the given age updated abstract structure and returns the updated concrete structure.

\Cref{lst:boom_testing} shows how Boomerang allows for unit testing.
We use the concrete structure defined previously to test the get function and an updated abstract structure \code{a1} (with Bob's age incremented) to test the put function.
\begin{lstlisting}[language=boomerang,caption={Unit testing in Boomerang},label={lst:boom_testing}]
test compsA.get c = ?

Test result:
"Alice, 28
Bob, 21
Charlie, 21"

let a1 : string =
  Bob Lopes, 22
  Alice Lopes, 28
  Charlie Martins, 21

test compsC.put a1 into c = ?

Test result:
"Bob, 22, UCP
Alice, 28, FCT
Charlie, 21, FCT"
\end{lstlisting}
As expected, the update to Bob's age is propagated to the concrete structure returned by the test.
However, the updated abstract structure \code{a1} has the first two lines swapped in relation to the concrete structure, meaning that, the update of the concrete structure leads to an unwanted update without a \emph{dictionary lens}.
If a \emph{dictionary lens} is not used in the update, the updated concrete structure will as described in \cref{lst:boom_no_dict}.
\begin{lstlisting}[language=boomerang,caption={Effects of no dictionary lens in Boomerang},label={lst:boom_no_dict}]
Test result:
"Bob, 22, FCT
Alice, 28, UCP
Charlie, 21, FCT"
\end{lstlisting}
As the test described result shows, without a \emph{dictionary lens}, the college name will be swapped as well, because we are matching in positional alignment and the first line is different in each structure, thus updating Bob's college with Alice's college and vice versa.

% We will define a structured way of defining contexts and views over data that are local to the contexts.
% We need bidirectional transformations to select and update data from sources to views and back.
