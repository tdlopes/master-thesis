\section{Basic Session Mechanisms}

In web applications, a session is a semi-permanent interactive data interchange, between two or more communicating devices, or between a computer and a user~\cite{Session}.
A session is established at a certain point in time, and has an expiration date.
An established session may involve more than one message in each direction.
Usually, a session has an associated state, meaning that at least one of the communicating parts needs to store session data in order to communicate.
Establishing a session is one of the basic steps to performing a connection-oriented communication.
In the next section we will study some of the session mechanisms used in providing state to communications in stateless web and Internet protocols.

\subsection{Cookies and tokens}

An \gls{http} Cookie, or just Cookie, is the most primitive mechanism, embedded into the \gls{http} protocol, to store small amounts of information on the user's browser.
Cookies are designed as a reliable mechanism for websites to persist user's information on the browser and recognize the user in a later interaction.
The browser sends the Cookies back to the respective sites every time the user accesses them.
Cookies are set in \gls{http} requests (response by a server to a request) through a Set-Cookie header which instructs browsers to store the data in a Cookie.
Afterwards, the cookie data is sent along in every request made to the same server in the form of a Cookie \gls{http} header.
Additionally, an expiration date, and restrictions to a specific domain can be specified.

Perhaps the most important function a Cookie performs in the modern web is supporting authentication between requests, a form of stateful communication between client and server.
Websites send back to the client some unique user information, usually a server-generated token, allowing the website to check in future requests if a user is authenticated or not and respond accordingly to the identified user.

\subsection{Authentication}

Authentication is the act of verifying an identity, more precisely in web and cloud applications, the identity of a user~\cite{Authentication:Nigel:Book,Authentication:Wiki:Online}.
There are three existing authentication factors in the process of identifying a user: knowledge factor, ownership factor, and inherent factor.
Knowledge factors include elements that a user knows (e.g. password, security question).
Ownership factors include elements that a user has (e.g. tokens).
Inherent factors include elements that a user is or does (e.g. fingerprint, voice, bio-metrics, signature).
For a positive authentication at least elements from two factors are required.
Authentication in web and cloud applications is commonly performed using a two-factor authentication with something the user has (e.g. username, id) and something the user knows (e.g password).

\gls{http} supports basic schemes for authentication like basic access authentication (providing a username and a password) and digest access authentication (applies an hash function before sending the credentials over the network).
These mechanisms operate via challenge-response mechanisms in which servers identify and issue challenges before answering to requests.
\gls{http} also allows the definition of separate authentication scopes under one root \gls{uri}, called authentication realms.

Another approach to authentication in the modern web is authentication using third-party services, eliminating the need for application developers to build their own ad-hoc login systems, allowing users to login to multiple unrelated services with possibly the same identity and credentials.
OpenID~\cite{OpenID} is an open standard for this decentralized authentication protocol which several large organizations either issue or accept (e.g. Microsoft~\footnote{https://www.microsoft.com}, Google~\footnote{https://www.google.com}).
Users just need to create an account in OpenID by selecting an identity provider and then sign onto any website that accepts it.

\subsection{Session Mechanisms}

\gls{http} is the foundation of data communication in the World Wide Web, and is a stateless protocol~\cite{RFC2616:1999:Online}, which means that, every time a communication between a server and a client ends, the information about the session is lost from the communication stack.
Sometimes it is convenient to maintain session related data, for example, to avoid asking for a password every time a client makes a request to the server, to keep track of a shopping cart over multiple requests to the server.
In order to maintain session data there are two obvious possibilities: Server Side web sessions and Client Side web sessions.

In a Server Side web session implementation, session information is stored on the server and a token is used to uniquely identify the session.
The token is either explicitly stored in an \gls{http} Cookie on the client browser or, explicitly sent as a parameter in the request also known as \gls{url} Rewriting.
Each request carries the Cookie to the server so it can match the request with the right session information in the server.

On the other hand, in a Client Side web session implementation, \gls{http} Cookies are used to directly store the session information on the client.
Cookies are automatically carried over on each request to give the server all the information needed about the session.

To show how these two styles are commonly implemented in frameworks we consider the case of Java Servlets~\footnote{http://docs.oracle.com/javaee/6/tutorial/doc/bnafd.html}, which mainly follows a Server Side web session style through the java interface HttpSession~\cite{HttpSession:JavaEE5:Online}.
When a client makes a request to a servlet, the request object (HttpServletRequest java object~\cite{HttpServletRequest:tomcat55:Online}) provides a method (\textit{getSession}) that retrieves, or creates, an HttpSession object to manipulate session information on the server.
By default the server maintains the HttpSession objects in a map, meaning that if the server goes down, all the session information is lost.
However, servers can be configured to persist session data to disk.
To uniquely identify each HttpSession object in the map, an \gls{http} Cookie (named JSESSIONID) containing a unique identifier (generated by the servlet), is kept on the client.
In this way, when a client makes a request with an \gls{http} Cookie named JSESSIONID, the HttpSession object that corresponds to that particular session is returned by the \textit{getSession} method, and with it the server can manipulate the session information for that particular client.
Additionally, Java Servlets also implements Client Side web sessions by allowing the creation and manipulation of \gls{http} Cookies kept on the client to store any kind of session information.

Aside from Server Side (with or without \gls{url} Rewriting) and Client Side web sessions, an alternative, and well known approach for storing session information is also commonly used.
In this approach, hidden fields are inserted in web pages to store session information related to the client accessing the web page.
This technique obviously raises security concerns, since, even thou they are hidden in the page, the source of the web page can be inspected to uncover its content.
The same concerns apply to \gls{http} Cookies, which are stored in the browser but can be viewed manually, or even stolen if given the required access to the machine.
In both \gls{http} Cookies and hidden field storage of session data, cryptography can be employed to mitigate these security concerns.

\subsection{Analysing Session Mechanisms}

While Server Side sessions are usually efficient and secure, in high-availability systems with no mass storage it becomes difficult to maintain efficiency.
Although limiting the number of clients accessing the server severely weakens the availability of a server, it makes it possible to reserve a portion of the \gls{ram} for storage of session data to tackle the absence of mass storage.
In contrast, Client Side sessions can mitigate the weight of storing and loading all session information on the server in high-availability systems, however, as previously mentioned data stored in the client is vulnerable to tampering.
In order to increase security of session data stored on the client, the server must be the only location to initiate a valid session, as well as the only system able to interpret and manipulate the data.
To guarantee confidentiality and integrity requirements a variety of cryptography methods can be used.
A clear problem of storing session information on the client is the size of the data being transmitted between the client and the server with every request, which is aggravated by the limitations that some browsers impose on the size and number of Cookies each site can store on the client.
To improve efficiency in carrying large amounts of session data and meet the limits of browsers, servers may compress data before creating a Cookie and decompressing it when the Cookie is received in a future request.

Many frameworks already provide at least either Server Side sessions or Client Side sessions in the most basic form without giving too much increased security.
While it is possible to code all the increased security discussed previously, it is an extensive process, prone to mistakes.
To make it simpler and safer to develop applications with sessions, a framework could infer what information needs to be sent to the client and, be able to tell when certain security invariants are being broken by knowing what information should not be sent to the client.
The only choices the developer should have to think about are the configuration choices of the employed mechanisms and perhaps where information is allowed to flow by explicitly marking data with annotations for security purposes.
However, the underlying mechanisms used by the framework should guarantee all the confidentiality and integrity requirements without compromising efficiency.
