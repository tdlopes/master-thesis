% !TEX root = ../../thesis.tex

\section{Base Language}

In this section, we briefly overview the new language mechanisms. In the subsequent sections, we give a more detailed explanation of each one of the introduced mechanisms and how their composition can be used to provide more sophisticated patterns.

\begin{lstlisting}[caption={Module mechanisms},label={lst:module_mechanisms}]
module Public {
  var welcome = "Welcome"
}

module User<string name, number age> with Public {
  var msg = welcome@Public ++ " " ++ name
  var myData = 0

  module Adult when(age >= 18) {
    import myData as data
    def inc = action { data := data + 1 }
  }
}
\end{lstlisting}

\paragraph{Session identity}
The ability to identify users is one of the basic requirements in most web frameworks, and it is also the foundation on which authentication mechanisms are built upon.
%
We explicitly introduce this basic mechanism in the language through a unique identifier for each connecting user (\gls{usid}).
The \gls{usid} is denoted by the reserved identifier \code{usid}.
%

\paragraph{Module introduction}
Modules define hierarchical and isolated environments, allowing for the explicit sharing of data between modules.
Modules are defined through the \code{module} definition constructs.
%
For instance, \cref{lst:module_mechanisms} shows how modules can be defined.
In the example, we define the \code{Adult} module nested in the \code{User} module to create a hierarchy.
%

\paragraph{Data Sharing}
Expressions in a module are defined with relation to an environment, which includes all definitions of that particular module.
Hence, by default, the names in a module's outer context are inaccessible.
In order to use names defined outside a module, we can create a surrogate name, via an \code{import} operation.
%
In \cref{lst:module_mechanisms}, the \code{Adult} module imports the \code{myData} name and uses the local alias \code{data} to define the action named \code{inc}.
%
An \code{import} operation defines a bidirectional view based on the well established concept of lenses~\cite{Steckermeier:Lenses}.
The created view is bound to the \code{data} name, and is set over the state variable \code{myData} outside the \code{Adult} module.
Any changes on either end, are propagated to the opposite end.
%
For instance, the defined action \code{inc} increments the value of the \code{data} name, thus any increment on the local alias \code{data} is propagated back to the original \code{myData} name.

\paragraph{Module parameterization and access control}
%
We introduce module parameters as a means of indexing a module state with relation to a set of parameters.
In \cref{lst:module_mechanisms}, we define the \code{User} module with a set of parameters: \code{name}, and \code{age}.
Thus, for each combination of the parameters, there is a different associated \code{msg}, and \code{myData} state.
%
Accessing a module name is done through the \gls{rest} \gls{api} described in \cref{ssec:runtime_support_system,sec:architecture}.
For example, accessing the \code{welcome} name in the example is done with a \textit{GET} request to \textit{/Public/msg}.
%
To restrict access to a module we also introduce guard conditions to the \code{module} definition constructs.
In the example we define such a condition for the \code{Adult} module, where access to the module is only granted for values of \code{age} greater than \code{18}.
%

\paragraph{Module inheritance}

Module inheritance is introduced to allow modules to fully access another module.
%
Using names from the inherited module is done through module identifiers, which are names appended to the module name with a \code{@} symbol.
%
The example in \cref{lst:module_mechanisms} defines an inheritance of the module \code{Public} in the module \code{User}, allowing the expression of \code{msg} to use the name \code{welcome} from the \code{Public} module.
\FloatBarrier
