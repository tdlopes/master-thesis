% !TEX root = ../../thesis.tex

\subsection{Inheritance}
\label{ssec:inheritance}

In this section, we introduce an inheritance mechanism to the language as a means of giving a module access to another module.
The next step in the development of our running To-do example application is to add a group environment for authenticated users to share information, which we will build with the help of module inheritance.

First, we define the state variable \code{groups} in the global environment, populated with the data found in \cref{app:groups}.
With the new information stored, we want each user to have a list of the groups he can access.
\Cref{lst:user_list_groups} redefines the \code{User} module of our running example application, with the help of a function that determines if a \code{string} is contained in a list of records that have a field \code{name}.
\begin{lstlisting}[caption=Group listing in the user page,label=lst:user_list_groups]
def listContains list name =
  match
    get item in list
    where item.name == name
  with
    u::us => true
  | [] => false

@User {
  import listContains
  from group in groups
    where (listContains group.users username)
    import group as groups
}
\end{lstlisting}
The function \code{listContains} is imported to help create a view that filters the newly defined \code{groups} collection, and returns the list of all the groups a user can access.

Now consider the module definition \code{Group} in \cref{lst:group_module} which inherits the module \code{User}.
\begin{lstlisting}[caption=Group module definition,label=lst:group_module]
module Group<string groupName, *usid> with User(usid)
  when(listContains groups@User groupName) {}
\end{lstlisting}
As seen in the example, a module inherits another module through the keyword \code{with}, followed by the inherited module name.
If the inherited module has a non empty set of parameters, the host module (module \code{Group}) must provide arguments to the inherited module parameters when the inheritance is defined.
We provide the \code{Group} module \gls{usid} parameter as the argument expression for the inherited module \code{User} \gls{usid} parameter.
This means that, when a user accesses the \code{Group} module, the \code{User} module state that is accessible to the user depends on the given \gls{usid} parameter.
Additionally, because the \code{User} module has an access condition, any access to the \code{Group} module must first test the inherited module condition.
Accessing an inherited name inside the host module is done through the appendage of the inherited module name with the \code{@} symbol to the inherited name.
We also define an access condition for the \code{Group} module using the inherited view name \code{groups@Users}, and test if a given \code{groupName} parameter is contained in it..

Next, we want each group to keep track of a to-do list, as well as a page to execute actions over each task.
The \cref{lst:group_todos} defines a simplified, non-styled, example~\footnote{A working example can be found in the work space at \url{http://live-programming.herokuapp.com/dev/gCkg7} with the authentication page at \url{http://live-programming.herokuapp.com/app/gCkg7/Public/page}} of the TodoMVC application built on top of our running example application.
\begin{lstlisting}[caption={Simplified, non-styled, TodoMVC application for groups of users},label=lst:group_todos]
@Group {
  var todos = [{
    id: 0,
    text: "Welcome to group " ++ groupName,
    done: false
  }]

  def size = foreach(todo in todos with y = 0) y+1

  def addTodo text = action {
      insert { id: size, done: false, text: text }
      into todos
    }

  def deleteTodo id = action {
      delete todo in todos
      where todo.id == id
    }

  def toggleComplete id = action {
      update todo in todos
      with { id: todo.id, done: not todo.done, text: todo.text }
      where todo.id == id
    }

  def todoItem todo =
    <li>
      <checkbox type="checkbox" value=(todo.done)
        docheck=(toggleComplete todo.id)
        douncheck=(toggleComplete todo.id)
      />
      <label>(todo.text)</label>
      <button doaction=(deleteTodo todo.id)>"Delete"</button>
    </li>

  def page =
    <div>
      <header>
        <h1>groupName</h1>
        <input placeholder="What needs to be done?" onenter=(addTodo) />
      </header>
      <section>
        <ul>(map (todo in todos) todoItem todo)</ul>
      </section>
    </div>
}
\end{lstlisting}
In the example we define three actions to manipulate the \code{todos} store:
\begin{itemize}
\item \code{AddTodo} adds a new to-do to the list with a given text
\item \code{deleteTodo} deletes a to-do, identified with a given \code{id}
\item \code{toggleComplete} toggle a to-do completed state
\end{itemize}
To keep track of how many tasks are in a to-do list, we define \code{size} that counts the elements in the \code{todos} collection.
We also defined the \code{todoItem} function, that, given a to-do item returns an HTML value displaying the to-do text, a checkbox to change the completed state of the task, and a button to delete the item.
The defined \code{page} allows the user to input a to-do by pressing enter through the attribute \code{onenter}.
This attribute takes a function with a \code{string} parameter, and when the \textit{enter} is pressed, the input value is used as the argument of the function (\code{addTodo}).
Finally, in the \code{page}, we iterate over the \code{todos} storage, and for each to-do we call the \code{todoItem} function in order to display each to-do with an HTML value.

Notice how the example does not make use of any of the inherited names except in the definition of the module access condition (because it is a delayed expression), and recall the problematic behavior of non-delayed expressions in a shared environment previously described in \cref{line:delayed_vs_stored}, \cpageref{line:delayed_vs_stored}.
With the inheritance mechanism introduced this behavior is now experienced when using inherited names on non-delayed expressions defined in the host module.
To demonstrate this behavior, consider the addition of a footer in the group \code{page} in which we display the authenticated user name associated with the given \gls{usid}.
\Cref{fig:footer_duality} shows the example with the added footer view with two authenticated users, Alice and David, where David was the first to authenticate and access the group page.\label{line:duality_issue}
\begin{figure}
\centering
\begin{minipage}{0.5\textwidth}
\centering
\includegraphics[width=1.7in]{group_page_david}
\caption{Alice's view}\label{fig:group_page_alice}
\end{minipage}%
\hfill
\begin{minipage}{0.5\textwidth}
\centering
\includegraphics[width=1.7in]{group_page_david}
\caption{David's view}\label{fig:group_page_david}
\end{minipage}
\caption{Group page with footer duality issue}
\label{fig:footer_duality}
\end{figure}
The group \code{page} is a non-delayed expression with a stored value for each group, and using a name such as the \code{username@User}, which depends on the \gls{usid} of the requesting authenticated user, causes the group \code{page} to evaluate with the corresponding authenticated user name.
This is due to the fact that the \code{Group} name is only indexed by the \code{groupName} parameter, while the inherited \code{User} module is indexed by the \gls{usid}.
Furthermore, changing the \code{Group} module \gls{usid} parameter to an indexing parameter, means that each user has different to-do list, even thou the added footer would correctly display the value of \code{username} for each user.
Our goal however, is to have a to-do list for each group while allowing the free usage of inherited names, and so the next section introduces a new mechanism that allows us to deal with the recurring non-delayed expressions behavior in shared environments.
\FloatBarrier
