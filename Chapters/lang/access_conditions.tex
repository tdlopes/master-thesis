% !TEX root = ../../thesis.tex

\subsection{Access conditions}
\label{ssec:access_conditions}

By default, a module state is accessible by any user.
However, in a web application, it is almost always required to have controlled access to a given state.
For example, a user profile is only accessible to an authenticated user, or a group profile to an authenticated user that has access to the group.
To tackle this need, we introduce a mechanism for modules to test a given access condition and determine whether a user can access a module state or not.
We map this module condition directly to a wrapper delayed expression through a function which takes a \emph{dummy} parameter.
This allows us to create a delayed expression which can be evaluated for each request without storing state of the result, just by using core language constructs.

Our growing To-do application requires controlled access to each group, thus we will expand upon it as we further explain the access conditions mechanism.
In \cref{ssec:parameterization} we finished a page where the stored users from \cref{app:users} can authenticate themselves.
Next, we will expand the application with a user page where the groups he can access are listed using the group seed data from \cref{app:groups}.

To set a module condition we introduced the \code{when} operator to modules, which takes an expression that must have a \code{boolean} type.
By default, a module defined without a condition is given one with the \code{true} literal.
The condition expression environment is composed of the module's parent environment and module parameters.
Each request to a module state must first evaluate its access condition and test if the result is true, or false.
As explained in \cref{ssec:parameterization}, accessing a module state requires the provision of arguments for the module parameters, which populate the environment in which the condition is tested.
This guarantees that the evaluation of the condition has all the module parameters in the environment.

\Cref{lst:user_module} shows the definition of an indexed module with an access condition, using the previously declared \code{authenticated} function in \cref{sec:sessions_and_auth}.
\begin{lstlisting}[caption={User module definition},label={lst:user_module}]
module User<usid> when(authenticated usid) {
  import logout
  from user in authenticatedUsers
    where user.token == usid
    import first user.name as username
    default {name:"",token:""}

  def page =
    <div>
      <h1>("User: " ++ username)</h1>
      <button
        doaction=(logout)
        data-redirect=(workspace_path ++ "Public/page")
      >"Log out"</button>
    </div>
}
\end{lstlisting}
The defined \code{module User} is indexed with the \gls{usid} parameter in order to create an isolated environment for each user, similar to \cref{lst:user_module} in \cref{ssec:parameterization}.
Each access to the \code{module User} will test if the \code{authenticated} function for a given \gls{usid}, evaluates to \code{true}.
With this module, each authenticated user has a different state.
In the new \code{User} module we define the \code{username import}, similar to the way we do in \cref{lst:usid_module_auth}, to view the name associated with an authenticated user.
Finally we define an HTML page that displays the \code{username}, and allows the user to log out and be redirected back to the log in page.

In \cref{lst:page_redefinition}, we redefine the \code{page} in the \code{Public} module, previously defined in \cref{lst:usid_module_auth}, and add a link to the user's page (\cref{lst:user_link}).
\begin{lstlisting}[caption={Public module redefinition},label={lst:page_redefinition}]
@Public {
  def page =
    <div>
      (if not authenticated usid then
        <div>
          <div>
            <input type="text" placeholder="username" id="name"/>
            <input type="password" placeholder="password" id="password"/>
          </div>
          <button doaction=(authenticate #name #password)>"Log In"</button>
        </div>
      else
        <div>
          <h1>("Welcome " ++ currentUser)</h1>
          <a href=(workspace_path ++ "User/page")>"Go to your page!"</a>*@\label{lst:user_link}@*
          <button doaction=(logout)>"Logout"</button>
        </div>)
    </div>
}
\end{lstlisting}

With these modifications, we have a boilerplate for user authentication~\footnote{A working example can be found in the work space at \url{http://live-programming.herokuapp.com/dev/H36KP}, with the authentication page at \url{http://live-programming.herokuapp.com/app/H36KP/Public/page}} on which we will build the rest of our running To-do list application.
\FloatBarrier
