% !TEX root = ../../thesis.tex

\subsection{Module Parameterization}
\label{ssec:parameterization}

So far, modules isolate data in a shared environment giving a single state equal to all users.
In this section we introduce a parameterization mechanism to index modules.
In an indexed module, each state is indexed with a set of parameters defined with the module.
To index a state, we use the previously introduced indexed lenses in \cref{sssec:indexed_lens}.
Each name defined in a module, is mapped directly to a \code{var}, or \code{def}, operation as described in \cref{sec:modules}, but in an indexed module the value stored for each name is instead an indexed lens.
The concrete state (default expression) of an indexed lens is given by the declared expression of a \code{var}, or \code{def}, commands and the lens index is given by the indexing parameters of the module.
With this, each declared expression can have different states for different combinations of arguments.
\Cref{lst:module_params} shows how module parameters can be declared.
\begin{lstlisting}[caption={Module defined with parameters},label={lst:module_params}]
module Room<number room, string *name> {
  var message = "Welcome to room " ++ str room
}
\end{lstlisting}
Every parameter requires a type annotation, and only basic types (string, number and boolean) are valid.
A parameter can also be defined as a non-indexing parameter with the \code{*} character prepended to the parameter name.
Non-indexing parameters are not used to index a module state, thus they are also not used in indexing lenses of the module.
For example, in the module \code{Room} in \cref{lst:module_params}, two users can access the same room state while providing different names through the parameter \code{name}.
Due to their non-indexing nature, the scope of non-indexing parameters is limited to mechanisms that will be introduced later in \cref{ssec:access_conditions,ssec:inheritance}.
In contrast, the scope of indexing parameters is the whole module.

In \cref{lst:module_params}, we have a state variable \code{message} that depends on the indexing parameter \code{room}.
The expression of the \code{message} is stored as the default value of an indexed lens.
When an index is accessed, the expression is evaluated with the provided arguments from the request to the module state.
The value is stored in the given index (formed with the \code{room} argument), resulting in every room having a different \code{message} state.

When a dependency of an indexed lens changes, the reactive nature of the language updates all stored values by re-evaluating the lens default expression for each index.
Because the index is constructed from the module arguments, each index is deconstructed to populate the module parameters in the evaluation environment.

The \gls{usid}, being itself an indexation of connecting users, is a prime candidate for a parameter in modules to index module states for each user.
We provide a language syntactic sugar when defining a \gls{usid} parameter to omit the type annotation, for example, \code{module User<string name, usid> \{ \}}.
In the example we define the \code{module User} with the \gls{usid} as a parameter, which is internally transformed into a \code{string usid} parameter.
This creates an isolated environment for each user in the \code{module User} state.
For safety, when accessing a \gls{usid} indexed module state, the server automatically populates the module arguments with the user's \gls{usid}.

Recalling our running example application last expanded upon in \cref{lst:auth_in_module}, a problem was keeping the application from growing.
The application wouldn't allow for multiple users to authenticate at the same time due to the fact that there was only one state for the \code{page}.
Indexing the \code{Public module} with the \gls{usid} parameter provides each user with an authentication page state with the \gls{usid} as its index.
This produces the desired authentication application as seen in \cref{lst:imp_auth} where the page function was made in function of a given token.
This new approach keeps the \gls{usid} safe, and the page value does not need to be re-evaluated for each request by the same user due to the nature of the indexed lens.
\Cref{lst:usid_module_auth} redefines the \code{module Public}, created in \cref{lst:auth_in_module}, using the \gls{usid} as a parameter.
\begin{lstlisting}[caption={Importing outside names},label={lst:usid_module_auth}]
// Data stores
// Authentication function
// Helper functions and actions

module Public<usid> {
  import authenticate
  import authenticated
  import logout
  from user in authenticatedUsers where user.token == usid
    import first user.name as currentUser
    default {name:"",token:""}

  def page =
    <div>
     (if not authenticated usid then
       <div>
         <div>
           <input type="text" placeholder="username" id="name"/>
           <input type="password" placeholder="password" id="password"/>
         </div>
         <button doaction=(authenticate #name #password)>"Log In"</button>
       </div>
     else
        <div>
          <h1>("Welcome " ++ currentUser)</h1>
          <button doaction=(logout)>"Logout"</button>
        </div>)
    </div>
}
\end{lstlisting}
The parameterization of the module, allows each user to see the \code{page} evaluated with his own \gls{usid} even when another user is authenticated.
As \cref{lst:usid_module_auth} shows, we use the lenses introduced in \cref{sssec:record_lens} to replace the previous \code{userFromId} imported function.
Instead of using a function, now we import the authenticated user name as \code{currentUser}.
The new \code{currentUser import} filters the \code{authenticatedUsers} collection with the user's \gls{usid}, and takes the \code{name} field of the first record matched.
Since the second branch of the \code{page} is only evaluated for authenticated users, we are guaranteed to never show the \code{default} value of the \code{currentUser}.

With an authentication mechanism implemented~\footnote{A working example can be found in the workspace at \url{http://live-programming.herokuapp.com/dev/LsT5t} with the authentication page at \url{http://live-programming.herokuapp.com/app/LsT5t/Public/page}} in our language we can start the next step in our To-do list example application mentioned at the start of \cref{ch:language_based_model}.
\FloatBarrier
