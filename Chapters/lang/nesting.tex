% !TEX root = ../../thesis.tex

\subsection{Module Nesting}
\label{ssec:nesting}

In this section, we introduce module nesting.
Nested modules create a hierarchy, thus, access to an inner module must first test the access conditions of all outer modules.
This means that, a nested module requires at least the same set of parameters as the outer modules in order to evaluate any outer conditions that might use parameters.
Thus, by default, a nested module automatically inherits the set of parameters of the closest outer module, which we will call \textit{nesting} parameters.
However, the \textit{nesting} parameters can be overridden for the nested module, meaning we can change a parameter from indexing to non-indexing and vice-versa without changing the actual outer module indexing parameters.
New parameters can also be added to the nested module.

Consider the module \code{Group} redefinition in \cref{lst:module_nesting}, which follows the development of our running To-do application.
We want to limit the \code{deleteTodo} action to administrators of each group using module nesting.
\begin{lstlisting}[caption={Module nesting},label={lst:module_nesting}]
@Group {
  from group in groups
    where group.name == groupName
    import first group.users as members
    default {name:groupName,users:[]}

  def isAdmin name =
    match
      get member in members
      where member.name == name
    with
      m::ms => m.admin
    | [] => false

  module Admin<string *groupName> when(isAdmin username@User) {
    import todos

    def deleteTodo id = action {
      delete todo in todos
      where todo.id == id
    }
  }
}
\end{lstlisting}
First, we defined a filtered view of the \code{groups} store to extract the members list of a given group containing all the information about each member of the group.
Then, we define the \code{isAdmin} function to test if a given member is an administrator in the group.
To create the administrator role, we define a nested module in the \code{Group} module, and override the \textit{nesting} parameter \code{groupName} to a non-indexing parameter.
We keep the \gls{usid} \textit{nesting} parameter as a non-indexed parameter since we don't have any state that needs to be indexed in the \code{Admin} module.
The \code{Admin} module is also defined with an access condition, with the help of the previously defined function \code{isAdmin}, to only allow access to administrators of the group.
Finally, we import the \code{todos} store to redefine the \code{delete} action from the \code{Group} module inside the \code{Admin} module.
The composition of mechanisms illustrated in the example creates a a non-indexed, isolated, and conditioned environment (the module \code{Admin}), inside an already existing isolated, and indexed environment (the module \code{Group}).

Next, we would like to display the administrator actions in the group page defined in module \code{Group}, but only when the user is an administrator.
To allow this kind of checked access to a module defined in another module's environment, we introduce the \code{In-Then-Else} operation.
This new operation is similar to the \code{If-Then-Else} statement, except instead of providing a condition to test, we provide a module name and arguments for each module parameter in order to test the given module access condition.
\Cref{lst:in_then_else} redefines the \code{todoItem} function, previously defined in \cref{lst:group_todos}.
\begin{lstlisting}[caption={Checked module access with In-Then-Else},label={lst:in_then_else}]
@Group {
  def todoItem todo =
    <li>
      <checkbox type="checkbox" value=(todo.done)
        docheck=(toggleComplete todo.id)
        douncheck=(toggleComplete todo.id)
      />
      <label>(todo.text)</label>
      (in Admin(groupName, usid) then
        <button doaction=(deleteTodo todo.id)>"Delete"</button>
       else <span></span>)
    </li>
}
\end{lstlisting}
We use a \code{In-Then-Else} statement with the \code{Admin} module as the target, and provide an argument for each one of the \code{Admin} module parameters.
In the first branch, where the \code{Admin} module condition test was positive, we display the delete button for the task.
The second branch simply shows an empty \code{span} \gls{html} element.

The composition in the example application\footnote{Working example in the work space at \url{http://live-programming.herokuapp.com/dev/5lYB5}, with the authentication page at \url{http://live-programming.herokuapp.com/app/5lYB5/Public/page}} still does not allow the inherited user state in any non-delayed expression of the \code{Group} module to have the desired result just as is previously described in \cref{line:duality_issue}, \cpageref{line:duality_issue}, and in \cref{line:delayed_vs_stored}, \cpageref{line:delayed_vs_stored}.
Additionally, because the defined \code{page} in the module \code{Group} is only indexed by the \code{groupName} parameter each group only has a \code{page} value.
Thus, for each given \code{groupName}, if an administrator is the first authenticated user and subsequently the first to request the \code{page} name, the administrator actions will be available to all users due to his \gls{usid} being used in the evaluation of the non-delayed expressions to be stored.
However, with module nesting we can create a different composition in order to express the previous example without having the undesired behavior of non-delayed expressions.
\Cref{lst:duality_fix} shows this compositions.
\begin{lstlisting}[caption={Member module},label={lst:duality_fix}]
@Group {
  module Member<usid> with User(usid) {
    import todos
    import isAdmin

    module Admin<string *groupName, *usid> when(isAdmin username@User) {
      import todos

      def deleteTodo id = action {
        delete todo in todos
        where todo.id == id
      }
    }

    def size = foreach(todo in todos with y = 0) y+1

    def addTodo text = action {
        insert { id: size, done: false, text: text }
        into todos
      }

    def toggleComplete id = action {
        update todo in todos
        with { id: todo.id, done: not todo.done, text: todo.text }
        where todo.id == id
      }

    def todoItem todo =
      <li>
        <checkbox type="checkbox" value=(todo.done)
          docheck=(toggleComplete todo.id)
          douncheck=(toggleComplete todo.id)
        />
        <label>(todo.text)</label>
        (in Admin(groupName, usid) then
          <button doaction=(deleteTodo todo.id)>"Delete"</button>
         else <span></span>
        )
      </li>

    def page =
      <div>
        <header>
          <h1>groupName</h1>
          <input placeholder="What needs to be done?" onenter=(addTodo) />
        </header>
        <section>
          <ul>(map (todo in todos) todoItem todo)</ul>
        </section>
        <footer>
          <p>("Logged as: " ++ username@User)</p>
          <button doaction=(logout@User)
                  data-redirect=(workspace_path ++ "Public/page")>
            "Log out"
          </button>
        </footer>
      </div>
  }
}
\end{lstlisting}
We define the module \code{Member} nested inside the \code{Group} module, and nest the previously defined \code{Admin} module inside the module \code{Member}.
We override the \code{Member} \textit{nesting} \gls{usid} parameter so that both the \code{groupName} and \gls{usid} parameters index the \code{Member} module.
Since the \code{Admin} module now inherits the \code{Member} module parameters, we override both the \code{groupName} and the \gls{usid} parameters as non-indexing parameters.
Then, we redefine all the previously defined module \code{Group} names inside the \code{Member} module, with the exception of the \code{todos} store.
The \code{todos} collection is instead imported from the \code{Group} module.

The composition created in \cref{lst:duality_fix}, provides the state of \code{page} as an indexed state by both the \code{groupName} and \gls{usid} parameters of the \code{Member} module.
The \code{todos} is imported to the \code{Member} module, which keeps the original collection indexed only by the \code{groupName} parameter.
It is also now possible to use the inherited names from the \code{User} module without the duality issue affecting non-delayed expressions, because the \code{Member} module is indexed with at least the same parameters as the \code{User} module (the \gls{usid} parameter).
For instance, in the redefined \code{page} name, we can add a footer to the \gls{html} page where we show the \code{username} value for each user, and a logout button.
Additionally, the definition of the inner module \code{Admin}, and use of the \code{In-Then-Else} statement, grants extra actions to some users.

\Cref{fig:groups_no_duality} illustrates two users, Alice and David, authenticated in different devices and viewing the to-do list for the family group.
\begin{figure}
\centering
\begin{minipage}{0.5\textwidth}
\centering
\includegraphics[width=2in]{group_page_alice}
\caption{Alice's view}\label{fig:group_page_alice_clean}
\end{minipage}%
\hfill
\begin{minipage}{0.5\textwidth}
\centering
\includegraphics[width=1.7in]{group_page_david}
\caption{David's view}\label{fig:group_page_david_clean}
\end{minipage}
\caption{Group page with different indexed states}
\label{fig:groups_no_duality}
\end{figure}
Since Alice is an administrator of the family to-do list group, the delete button is displayed as \cref{fig:group_page_alice_clean} shows.
In \cref{fig:group_page_david_clean} we see David's view, and because he is not an administrator the delete button is not displayed.
\Cref{fig:groups_no_duality} also shows that both views have their respective authenticated user names at the bottom.

To finish our running application~\footnote{Working example in the work space at \url{http://live-programming.herokuapp.com/dev/BluQ1}, with the authentication page at \url{http://live-programming.herokuapp.com/app/BluQ1/Public/page}}, we redefine the user page in the \code{User} module to update the groups list with new links because the \code{page} name in \code{Group} module as been moved to the inner module \code{Member}.
\cref{lst:new_links} redefines the user page.
\begin{lstlisting}[caption={User page updated links},label={lst:new_links}]
@User {
  def page =
    <div>
      <h1>("User: " ++ username)</h1>
      <button doaction=(logout) data-redirect=(workspace_path ++ "Public/page")>
        "Log out"
      </button>
      <h2>"Groups"</h2>
      <ul>
        (map (group in groups)
          <li>
            <a href=(workspace_path ++ "Group/Member/page/" ++ group.name)>
              (group.name)
            </a>
          </li>
        )
      </ul>
    </div>
}
\end{lstlisting}
\FloatBarrier
