% !TEX root = ../../thesis.tex

\subsubsection{Simple Lens}

We introduce simple lenses to create direct abstract states for given named concrete states.
The original name is stored in the lens, allowing each operation to execute over it.
The available transformations of a simple lens are as follows:
\begin{itemize}
\item \code{get}: retrieves the current value of the original name;
\item \code{put}: changes the current value in the original name to a new given value.
\end{itemize}

The simple lens supports the most basic \code{import} operation to view and modify a state directly.
\Cref{lst:simple_lens} shows an example of importing a name and defining an action to change its state.
\begin{lstlisting}[caption={Simple name import},label={lst:simple_lens}]
var data = [1,2,3,4,5]

module Public {
  import data as pubData

  var addData n = action { insert n into pubData }
}
\end{lstlisting}
In the example, the collection \code{data} is imported into the module \code{Public}, and given a new name, \code{pubData}.
If a new name is not given with the keyword \code{as}, the name of the import defaults to the original name.
We then define an action to insert a new value into the imported name.
The \code{addData} action concatenates the current value in the \code{data} name (retrieved with the \code{get} operation) with the given \code{n} value.
The result is then stored in the \code{data} name with the \code{put} operation of the lens.
The type system only accepts lenses over names defined with \code{var}/\code{def} operations and imported names in the closest outside environment of the module.

\subsubsection{Filtered Lens}
\label{sssec:filtered_lens}

The filtered lens is introduced to create a filtered abstract state of a given concrete state.
In this particular case, both the concrete state and the resulting abstract state are collections.
The abstract state is a collection consisting of all the elements in the concrete state that satisfy the given predicate.
The available transformations of a filtered lens are as follows:
\begin{itemize}
\item \code{get}: iterates over the concrete state with an accumulator collection which starts empty, and each element that satisfies the given predicate is added to the accumulator, which is then returned as the abstract state;
\item \code{put}: changes the current value in the original name to a new given value.
\end{itemize}

\Cref{lst:filtered_lens} shows an example of a filtering \code{import}.
\begin{lstlisting}[caption={Filtered import of a collection},label={lst:filtered_lens}]
var data = [1,2,3,4,5]

module Public {
  from n in data where n > 3
    import n as pubData

  var addData n = action { insert n into pubData }
}
\end{lstlisting}
First, we define \code{from n in data} to bind the name \code{n} to each element when iterating the \code{data} collection.
The predicate, \code{where n > 3}, matches elements in the \code{data} collection greater than 3.
\code{import n} declares that we want to import the each element as is.

In \cref{lst:filtered_lens}, the \code{get} operation returns the collection \code{[4,5]}.
The \code{addData} action produces the result the same way as the action in \cref{lst:simple_lens}, which is also stored in the name \code{data} through the \code{put} operation.
Delete and update actions combine their filter conditions with the \code{import} operation predicate in order to only delete elements pertinent to the abstract state.

\subsubsection{Filtered First Lens}

The \textit{filtered first} lens is introduced as an extension of the filtered lens in \cref{sssec:filtered_lens}, except the resulting abstract state is an element of the original collection instead of a collection of elements.
The abstract state is the first element of the original collection that satisfies the given predicate.
Because of the possibility of no matching element, the lens requires a given default value of the same type as the original collection elements.
The available transformations of a filtered first lens are as follows:
\begin{itemize}
\item \code{get}: the same selection as the filtered lens is executed, then if the resulting collection has elements, the first element is returned.
Otherwise, if there is an already stored value in the lens, the stored value is returned, alternatively the defined default value is returned;
\item \code{put}: if the abstract collection is not empty, all the elements that satisfy the predicate are updated with the new given value.
Otherwise, the new value is stored in the lens.
\end{itemize}

\Cref{lst:filtered_one_lens} shows how we can import the first element matched in a filtered collection.
\begin{lstlisting}[caption={Import first element of a filtered collection},label={lst:filtered_one_lens}]
var data = [1,2,3,4,5]

module Public {
  from n in data where n > 3
    import first n as pubData
    default 0

  var changeData i = action { pubData := i }
}
\end{lstlisting}
As the example shows, the \code{import} operation follows the almost the same structure as the filtered lens in \cref{lst:filtered_lens}.
With the exception being that, instead of importing all elements matched, we import only the first match with the keyword \code{first}.

In \cref{lst:filtered_one_lens}, the \code{get} operation of the lens would return the value \code{4} in the example.
The action \code{changeData}, when executed, calls the \code{put} transformation described above with the given result value of \code{i}.
For example, if we execute the action with \code{changeData 6}, the \code{put} operation would update all elements greater than \code{3} to \code{6}.
This behavior is due to the fact that collections are iterated in lens transformations, that is, elements are not accessed by position in the language, and so we adopted this solution in order to not produce erroneous behavior.
The \code{data} collection would be equal to \code{[1,2,3,6,6]}, and the \code{get} transformation would return the value \code{6}.
If, however, the filter was \code{n > 5}, the \code{get} operation would return the default value \code{0} due to no matching elements.
The given default value does not satisfy the predicate because the condition might contain names which can only be evaluated at run-time (like the \gls{usid}), thus the lens cannot know if the default value satisfies it.
The adopted solution is safe, but doesn't keep the user from defining inconsistent imports with this lens.
In this case, the action \code{changeData 6} would store the value \code{6} in the lens instead, meaning that subsequent \code{get} calls return the stored value \code{6}.
The value \code{6} is consistent with the predicate, but if we executed the action with the argument \code{2}, the stored value would be \code{2} even thou it is inconsistent with the condition.

\subsubsection{Record Field Lens}
\label{sssec:record_lens}

In this section we introduce a type of lens to compose with simple and filtered lenses.
This lens makes clear that an import declaration is defined by a lens or composition of lenses.
Depending on the composition, the concrete state can be a record if composed with a simple lens, or collection of records if composed with a filtered lens.
The available transformations of a record lens are as follows:
\begin{itemize}
\item \code{get}: first the composed lens \code{get} operation provides a result, and then, the record lens extracts the given field.
The extraction process depends on the result of the composed lens.
If the result is a record, the extracted field is returned as the abstract state.
If the result is a collection of records, the returned abstract state is a collection of extracted fields.
\item \code{put}: if the concrete state given by the composed lens is an object, then the field is replaced with the new given value and the \code{put} operation of the composed lens is fed the new object result.
Otherwise, if the composed lens produces a collection of records, then the field of each record is replaced with the given value, and the resulting updated collection is put back with the \code{put} operation of the composed lens.
\end{itemize}

\Cref{lst:record_lens} shows an example of how these compositions can be defined with the \code{import} operation.
\begin{lstlisting}[caption={Record field filtering with composed lenses},label={lst:record_lens}]
var data = { name: "App", settings:[] }
var users = [{name: "Alice", points: 18}, {name: "Bob", points: 33}]

module Public {
  // Simple lens composition
  import data.name as app *@\label{line:comp_simple}@*

  var change n = action { app := n }
}

module Game {
  // Filtered lens composition
  from user in users where user.points > 20  *@\label{line:comp_filter}@*
    import user.name as topPlayers
    default {name: "Dummy", points: 21}

  var addPlayer name = action { insert name into topPlayers }
}
\end{lstlisting}
We defined two compositions with the record lens: a simple lens, and a filtered lens.

\paragraph{Simple lens composition}
The record lens composition with a simple lens is defined just as a simple lens \code{import} in \cref{lst:simple_lens}, but instead of importing the object as is we import the given \code{name} field of the object, as seen in \cref{line:comp_simple} of \cref{lst:record_lens}.
The record lens concrete state is given by the simple lens abstract state of the record \code{data} name.
In the example, the \code{get} operation returns the \code{\"App\"} value.
The action \code{change} calls the \code{put} transformation of the record lens with the given \code{n} value, which creates the updated object that is then given to the \code{put} operation of the simple lens.

\paragraph{Filtered lens composition}
The record lens composition with a filtered lens is defined just as a filtered lens \code{import} in \cref{lst:filtered_lens}, but instead of importing each matching element as is, we import the given \code{name} field, as seen in \cref{line:comp_filter} of \cref{lst:record_lens}.
Additionally, the default value \code{\{name: "Dummy", points: 21\}} is defined with the \code{import} operation.
The default value is needed when inserting new elements into the lens abstract state in order to populate the rest of the fields in the new object.
The record lens concrete state is given by the filtered lens abstract state over the \code{users} collection, and each object field of the filtered collection is extracted to build the abstract state.
In the example, the imported \code{topPlayers} name returns the collection \code{["Bob"]}.
The action \code{addPlayer} creates a new object with the field \code{name} assigned the value of the \code{name} argument, and populates the field \code{points} with the provided default value \code{21}.
The new object is then passed onto the \code{put} transformation of the filtered lens, which inserts the object in the original collection.
%%%%%%
\begin{comment}
\paragraph{Filtered one lens composition}
The filtered one lens composition is defined just as a filtered one lens \code{import} operation in \cref{lst:filtered_one_lens}, but instead of importing the first matching element as is we import the given \code{points} field.
The record lens concrete state is given by the filtered one lens abstract state over the \code{users} collection, and the given \code{points} field is extracted from the resulting object as the abstract state of the lens composition.
In the example, the imported name \code{aboveMax} returns the value \code{33}.
The action \code{resetMax} updates the object returned
\end{comment}
%%%%%%%

\subsubsection{Indexed Lens}
\label{sssec:indexed_lens}

Indexed lenses support the state in module parameterization, a mechanism that is explained in \cref{ssec:parameterization}.
Indexed lenses are set over a map structure where each entry is a concrete state, and the lens \emph{moves} to an entry with a given index to apply the lens transformations.
We introduce indexed lenses to index concrete states into as many abstract states as the indexing key allows.
Unlike the previously introduced lenses, indexed lenses store the produced abstract states as concrete states for each given index, because producing an undetermined number of abstract states for every \code{get} operation is potentially costly.
\begin{figure}[htp]
\centering
\includegraphics[width=2.5in]{indexed_lens}
\caption{Indexed Lens structure}
\label{fig:indexed_lens}
\end{figure}
Each indexed lens is defined with a set of parameters which are used to produce an index key for an abstract state.
An index key is created by concatenating each given argument into a \code{string} to access a unique abstract state, just as a map structure as seen in \cref{fig:indexed_lens}.
A concrete state stored in an indexed lens is given by a default expression when defining the indexed lens.
The available transformations of indexed lenses are as follows:
\begin{itemize}
\item \code{get}: given a set of arguments, the lens combines them into a key, and the associated abstract state is returned. If the key lookup doesn't find an abstract state, the \code{create} operation is used to store a new abstract state and return it;
\item \code{put}: given an abstract state and a set of arguments, the lens combines the arguments into a key and updates the abstract state in the associated entry;
\item \code{create}: the default expression, stored in the lens as the concrete state, is evaluated for a given set of arguments and stored with the composed key in the associated entry.
\end{itemize}
