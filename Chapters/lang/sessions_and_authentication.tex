% !TEX root = ../../thesis.tex

\section{Sessions and Authentication}
\label{sec:sessions_and_auth}

\emph{Identification} refers to the act of being able to state a person or thing's identity, \emph{authentication} is the process of confirming that identity~\cite{Authentication:Nigel:Book}.
Nowadays most web applications need to identify devices in order personalize user experience, which they do by creating sessions between the server and the client.
To establish a session, the devices need to first have an identity.
To accomplish this, we introduced the Unique Session ID~\cite{USID:Online}.
The \gls{usid} is a server generated string token given to each unidentified device.
Each device is then responsible for storing the \gls{usid} and sending it attached in future communications, thus establishing a session.
An Internet browser, for example, uses \gls{http} Cookies~\cite{HTTPCookies} to store the given \gls{usid}.
In the context of the programming language, we expose the \gls{usid} through the reserved keyword \code{usid}.

The introduction of the \gls{usid} enables the server to identify users, thus allowing the definition of authentication mechanisms in the language.

We want to build a simple username/password authentication mechanism where each user has a password associated to him.
The authentication process will require the user to provide a name and a password.
If the given login information matches with the stored information the user is authenticated with the given name (assuming the username/password combination is only known to the user).
To build an authentication mechanism like this, we need a collection of users and a store to register authenticated users.
\begin{lstlisting}[caption={Data stores for an authentication process},label={lst:data_for_auth}]
table users {
  name: string,
  password: string
}
table authenticatedUsers {
  name: string,
  token: string
}
\end{lstlisting}
The \cref{lst:data_for_auth} shows the table \code{users} where the username/password combinations are stored, and the table \code{authenticatedUsers} to track authenticated users by associating the token of a user to a name.
We populate the user storage with the data found in \cref{app:users}.

In \cref{lst:auth_proc} we define our authentication function with two parameters, name and password, which constitutes a login function.
\begin{lstlisting}[caption={Authentication process},label={lst:auth_proc}]
def authenticate name password =
  match
    get user in users
    where user.name == name and user.password == password
  with
    user::rest =>
      action {
        insert {
          name: name,
          token: usid
        } into authenticatedUsers
      }
    | [] => action {}
\end{lstlisting}
It first performs a selection action over the \code{users} collection to retrieve a user with the given name/password combination, and matches the result with either a non-empty collection, or an empty one.
A non-empty collection means that a user was found, and so an action associating the user to a token is returned.
If the matched collection is empty then no user was found and an empty action is returned.
The returned action can later be attached to a login button to execute it.

With the authentication mechanism created, we now want to create a reactive page that shows a login form if the user is not authenticated, otherwise it shows the user name and allows him to log out.
To define this page we require some helper functions as seen in the \cref{lst:auth_page_helpers} for additional functionality.
\begin{lstlisting}[caption={Authentication page helpers},label={lst:auth_page_helpers}]
def logout =
  action {
    delete user in authenticatedUsers
    where user.token == usid
  }

def authenticated id =
  match
    get user in authenticatedUsers
    where user.token == id
  with
    user::rest => true
    | [] => false

def userFromId id =
  match
    get user in authenticatedUsers
    where user.token == id
  with
	  user::rest => user.name
	| [] => ""
\end{lstlisting}
Respectively, the first provides an action to create a logout button, the second is a function that checks if a given user is authenticated and the last one retrieves the name associated with an authenticated user.

With the helper functions, and actions, in place we can create the page shown in \cref{lst:auth_page}.
\begin{lstlisting}[caption={Authentication page},label={lst:auth_page}]
def page =
  <div>
   (if not authenticated usid then
     <div>
       <div>
         <input type="text" placeholder="username" id="name"/>
         <input type="password" placeholder="password" id="password"/>
       </div>
       <button doaction=(authenticate #name #password)>"Log In"</button>
     </div>
   else
  		<div>
        <h1>("Welcome " ++ userFromId usid)</h1>
        <button doaction=(logout)>"Logout"</button>
  		</div>)
  </div>
\end{lstlisting}
\Cref{fig:auth_page_1,fig:auth_page_2} show a flow of user authentication in the created application~\footnote{The full example can be found at \url{http://live-programming.herokuapp.com/dev/pP4ta}, with the application main page at \url{http://live-programming.herokuapp.com/app/pP4ta/page}.}, where a user is authenticated with a password and the page reacts to the successful operation by showing a welcome message with the user name.
\begin{figure}
\centering
\begin{minipage}{0.5\textwidth}
\centering
\includegraphics[width=1.5in]{auth_page_1}
\caption{User authentication page}\label{fig:auth_page_1}
\end{minipage}%
\hfill
\begin{minipage}{0.5\textwidth}
\centering
\includegraphics[width=1.5in]{auth_page_2}
\caption{Authenticated user page}\label{fig:auth_page_2}
\end{minipage}
\end{figure}

Even thou the authentication process is successful, the application can only sustain one authenticated user due to the shared global state in which the application is defined and the nature of non-delayed expressions.\label{line:delayed_vs_stored}
This example is important to show how non-delayed expressions, like the \code{page}, with a stored global value that is only re-evaluated when a dependency is changed behave in a shared environment.
In the example, the \code{page} name depends on the \code{authenticatedUsers} collection through the \code{authenticated} function which causes the page dependency on the state of the \code{authenticatedUsers} store.
The \code{page} evaluates either to a login page (\cref{fig:auth_page_1}) or a simple user page (\cref{fig:auth_page_1}) depending on whether the user is authenticated or not when the expression is evaluated.
If a user authenticates with the correct credentials the user name is associated with the given user \gls{usid} in the \code{authenticatedUsers} store, which in turn causes the \code{page} name to be re-evaluated for the user that requested the \code{authenticate} action.
When, for example, Alice authenticates with her credentials, the resulting updated state of the \code{page} name will show the view in \cref{fig:auth_page_2} and any other subsequent request for the \code{page} name will return the same stored value of \cref{fig:auth_page_2} for all users.
At this point, if the \code{logout} action is requested by Alice, then the page will be re-evaluated back to show \cref{fig:auth_page_1} and the \code{authenticatedUsers} store will be empty again.
However, if instead of Alice, a non authenticated user requests the \code{logout} action, it will not remove Alice from the \code{authenticatedUsers} store due to the requesting \gls{usid} not matching Alice's \gls{usid}.
Then, because the \code{page} still depends on the \code{authenticatedUsers} store, the \code{page} is re-evaluated with the non authenticated user \gls{usid} that requested the \code{logout} action, thus resulting in the login page in \cref{fig:auth_page_1} while Alice remains authenticated according to the \code{authenticatedUsers} store.
Alternatively, when the \code{page} state is \cref{fig:auth_page_2}, suppose the non authenticated user could authenticate directly through the framework console.
This action would produce the same reaction, updating the \code{authenticatedUsers} store by associating the new authenticated user with his \gls{usid} and in turn updating the dependent \code{page} name, but in this case the resulting stored view would be similar to \cref{fig:auth_page_2} but with the new authenticated user name while Alice is still authenticated.

With the behavior demonstrated in the previous example in mind, and since functions are delayed expressions that store no value after being called, it is possible to define a function that takes a \gls{usid} argument and returns an \gls{html} page.
The parameter \code{token} will then replace all \gls{usid} occurrences in \cref{lst:auth_page} as shown in \cref{lst:imp_auth}.
\begin{lstlisting}[caption={Authentication page through a function},label={lst:imp_auth}]
def page token =
  <div>
   (if not authenticated token then
     <div>
       <div>
         <input type="text" placeholder="username" id="name"/>
         <input type="password" placeholder="password" id="password"/>
       </div>
       <button doaction=(authenticate #name #password)>"Log In"</button>
     </div>
   else
  		<div>
          <h1>("Welcome " ++ userFromId token)</h1>
        <button doaction=(logout)>"Logout"</button>
  		</div>)
  </div>
\end{lstlisting}
With the page wrapped in a function with the \gls{usid} as a parameter, when Alice requests the page with her \gls{usid} as the argument the page will be evaluated with her \gls{usid}.
If she successfully authenticates, she will have a unique HTML page value refreshed only with the given \gls{usid} to reflect changes on the \code{authenticatedUsers} store.
As a result of this approach, only Alice will see the page in \cref{fig:auth_page_2} as expected, other non authenticated users can call the \code{page} function with their own \gls{usid} to get a unique \code{page} value with the view in \cref{fig:auth_page_1} to be able to authenticate.
This behavior duality between delayed expressions and non-delayed expressions in a shared environment is a recurring, and important, issue throughout this chapter.

This approach~\footnote{Working example of this approach can be found at \url{http://live-programming.herokuapp.com/dev/Ny7si}.} can then be applied to the rest of the application as we build it, that is, every state being in function of the \gls{usid} and/or other data.
However, having users provide the \gls{usid} manually to execute a function that returns data is not safe and user friendly since the user needs to find the Cookie storing the \gls{usid}, and provide it explicitly.
In order to provide a better way of isolating a state of users and groups of users while keeping the reactive properties of the language, illustrated in this example, we introduce the explicit declaration of modules and associated mechanisms in the next sections.
\FloatBarrier
