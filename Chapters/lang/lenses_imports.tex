% !TEX root = ../../thesis.tex

\subsection{Lenses and Imports}
\label{ssec:lenses_imports}

The \code{import} operation is used to create views over given named states that are defined in a parent environment, allowing a module to access its data or filter it.
By default the views are named after the imported name.

Going back to our running authentication example, we can now achieve what was established in \cref{sec:modules}, keeping the data stores and functions outside a module while the application page is defined inside a module.
Assuming the already defined data stores, functions, and actions, the \cref{lst:auth_in_module} shows the intended result by importing the four defined functions and actions that manipulate the data stores without exposing the sensitive information to the module state.
\begin{lstlisting}[caption={Importing outside names},label={lst:auth_in_module}]
// Data stores of *@\cref{lst:data_for_auth}@* with seeds from *@\cref{app:users}@*
// Authentication function of *@\cref{lst:auth_proc}@*
// Helper functions and actions of *@\cref{lst:auth_page_helpers}@*

module Public {
  import authenticate
  import authenticated
  import userFromId
  import logout

  def page =
    <div>
     (if not authenticated usid then
       <div>
         <div>
           <input type="text" placeholder="username" id="name"/>
           <input type="password" placeholder="password" id="password"/>
         </div>
         <button doaction=(authenticate #name #password)>"Log In"</button>
       </div>
     else
        <div>
            <h1>("Welcome " ++ userFromId usid)</h1>
          <button doaction=(logout)>"Logout"</button>
        </div>)
    </div>
}
\end{lstlisting}

We use an internal mechanism of lenses~\cite{Steckermeier:Lenses,Relation:Lenses:Proceeding,Lenses:Pierce:Paper} to support the import operations.
Imports map directly to \code{def} operations with the corresponding lens value.
A name defined with a \code{def} operation has reactive properties, which means that the stored import lens retains the reactive properties.
Lenses are views over a state that allow bidirectional transformations between a set of inputs (concrete states), and a set of outputs (abstract states).
A lens is comprised of two main operations as shown in \cref{fig:lenses}:
\begin{figure}[htp]
\centering
\includegraphics[width=4in]{lenses}
\caption{Lens \emph{get} and \emph{put} transformations~\cite{DLenses:Online}}
\label{fig:lenses}
\end{figure}
\begin{itemize}[noitemsep]
\item \code{get}: a forward transformation, from the concrete state to the abstract state;
\item \code{put}: a backwards transformation that takes an old concrete state and updates it with an updated abstract state;
\end{itemize}
It is also useful to have an operation that creates a concrete state from a given abstract state without an original concrete state.
This operation is called \code{create}.
The \code{create} operation can be achieved with the \code{put} operation by providing a default concrete state instead of an old concrete state, and update it with a given abstract state.

We introduced a number of different kinds of lenses that support the most common situations of importing and filtering data from a module to another, as well as use the composition of lenses to build more complex data filters.

In the following sections, we will describe in detail the kind of lenses and lens compositions operations we adopted and implemented. For each kind of lens we show a small example of the supported import operations.

\input{Chapters/lang/lenses}
\FloatBarrier
