% !TEX root = ../thesis.tex

\chapter{Introduction}
\label{ch:intro}

% What's the problem
Cloud development and virtualization of applications is becoming a crucial common practice in the software engineering industry~\cite{Cloud2009,VirtualizationAnalysis:Younge:Paper}.
%
Software-as-a-Service applications have become increasingly popular within the cloud~\cite{SaaS:Tsai:Paper,EngineerMTAs:Bikram:Paper}.
The development of these customizable \glspl{mta}, increases the reuse of code, but demands developer skills and attention to ensure data separation and privacy between users and tenants~\cite{ReportMT:Bezemer:Paper}.
%
There are many tools and frameworks that work at the system and database levels to help support many instances and views of an application, as well as share and replicate databases~\cite{ImplementingMTAs:Rouven:Paper,ApproachMTAS:Josino:Paper}.
%
The use of virtualization infrastructures like the Amazon AWS containers~\cite{Amazon:Containers:Online} is widely accepted, and heavily based on smart system management and virtual configuration tools.
However, application instances are many times separate machines with explicit sharing and coordination code.
%
The amount of complex hand-written code that keeps data and functionality separated comes with a high development, testing, validation and maintenance cost.
This long development process is prone to errors, and as the application scales so does the chance for errors.

% Why the problem is important
It is important to use sophisticated language based approaches in order to validate data separation between users and tenants.
But, more importantly, there is an increasing need for languages and tools that support the gradual development of software for a highly shared environment, both at the developer and user level.
%
Additionally, there should be language based mechanisms for defining (and validating) single user applications and then transform them, by adding only user management code, to a context of a multi-user application.

% What's your approach to the solution
In this thesis we build on top of a typed, reactive and gradual programming language and the corresponding programming environment.
The core language allows the definition of a set of data variables and active expressions associated to public names.
It allows for a type safe redefinition of a data variable or method, as well as the redefinition of the type of an expression.
This is obtained by keeping all data dependencies between names and preventing changes to propagate erroneously through the dependency graph.

We extend the language with a module abstraction to isolate data and functionality and module mechanisms that map to core language constructs.
We introduce module mechanisms such as: parameterization to index the isolated data and functionality; nesting to create module hierarchies with role based development in mind; guard condition mechanism to dynamically verify module access conditions and implement data privacy; and inheritance to give modules full access to inherited modules.
Data sharing is supported by lens~\cite{Lenses:Pierce:Paper,Relation:Lenses:Proceeding,Steckermeier:Lenses} based filters and sliced data sharing mechanisms between modules.
%
% Why is it interesting
With a language-based approach, the incremental and reactive properties of the core language are still present in the introduced abstractions.
The combination of the mechanisms that are presented in this thesis is a safe and powerful paradigm to design and evolve cloud and web applications.

% How is it illustrated and validated
We present a pragmatic programming language supported by a deployed prototype where several examples of applications illustrate this new programming paradigm.
We will show how authentication is easy to achieve, allowing the developer to build authentication mechanisms from scratch and use them throughout the rest of the development to validate data access dynamically.
With this new paradigm we will build a large To-do list application for groups of users to share tasks, with each user possibly having an administrator role in a group.
Each user will also be able to filter the tasks to customize his own view.
Traits like role based development, user tailored views, and group shared information are multi-tenant traits which we want to address with the combination of the introduced mechanisms.
With this application we will show how the introduced language abstractions speed up development, and simplify code.
We will also compare with modern frameworks with smaller examples to benchmark our solution.

\input{Chapters/intro/reactive_and_incremental_language}

\input{Chapters/intro/approach}

\input{Chapters/intro/contributions}

\input{Chapters/intro/structure}
