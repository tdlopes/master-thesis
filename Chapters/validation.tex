% !TEX root = ../thesis.tex

\chapter{Validation}
\label{ch:validation}

In this chapter we provide a small benchmark of our implemented language against the Meteor framework.
As presented in \cref{sec:related_frameworks}, Meteor has a synchronization process which automatically propagates changes with the \gls{ddp}~\cite{MeteorDDP:2016:Online} that allows for rapid prototyping and incremental development, closely resembling the reactive and incremental language that we extend in this thesis.
Additionally, Meteor is the most popular~\cite{HotFrameworks:Ranking:Online} and the most code efficient framework out of the ones studied in \cref{sec:related_frameworks}.
The target applications of the benchmark are the \glspl{mta}, as they are the target applications of the work done in the context of this thesis.
We host the developed example applications on Heroku~\cite{Heroku:Online}.
The benchmark metrics we use are code succinctness, and development costs.
We do not, however, compare the performances of both frameworks since the adopted prototype is visibly slower than Meteor when evaluating and providing an \gls{html} page after any update.
This visible performance discrepancy is due how the prototype handles \gls{html} values, which are fully re-evaluated after each change as opposed to only re-evaluating the relevant parts.
The low performance of the prototype is addressed as future work in the next chapter since it is a major factor in the viability and usability of any web framework.

First we will address the differences of authentication in a well established framework like Meteor and our language.
Then, we look at a simple wall application where any user can post a message anonymously, and transform it into a multi-group wall application thus comparing the efforts required by both frameworks.
Next, we take a closer look at the TodoMVC~\cite{TodoMVC:Online} multi-group application developed in both frameworks.
Finally, we end the chapter with some conclusions.

\section{Authentication}

In this section, we focus on the development costs of both frameworks by analyzing code re-usability and maintenance with the example of authentication mechanisms in mind.
Meteor, as a well established framework, has a package manager which provides around 590 packages~\cite{AtmosphereJS:Packages:Auth} with different implemented and ready to use authentication mechanisms.
In \cref{sec:sessions_and_auth} we defined a working simple authentication mechanism in our language, which is later used in \cref{ssec:access_conditions} to control access to an authenticated user environment.
The full simple authentication example code is listed in \cref{app:simple_auth}.
Even thou our language allows the developer to build authentication from scratch as well, it lacks security aspects like cryptography to create more secure authentication mechanisms.
Additionally, seeing as authentication mechanisms are at the core of most common web applications, it is crucial for a faster development that these mechanisms are easy to re-use and maintain.

Packaging and distributing smaller applications is essential for the faster development of applications.
Our framework, being a prototype, still has no way of packaging and distributing applications, such as the one we developed in \cref{sec:sessions_and_auth}.
However, re-using previously written code for a new application in our framework can be simple due to its ability to receive large chunks of code.
Utilizing this simple input of code is a good basis for a distribution method in the implementation of a future package manager for the current prototype.
With a package manager and more security aspects implemented into the language, it is then possible to build cryptography modules and authentication mechanisms, package them and distribute through the package manager.

\section{Simple Wall Application}

The benchmark in this section is focused on understanding the cost of developing an application and then transforming it into a multi-group application.
In \cref{lst:live_wall} we define the wall application~\footnote{A working example can be found at \url{http://live-programming.herokuapp.com/app/Br1ti/page}} in our language, where any user can post anonymously.
\begin{lstlisting}[caption={Multi-user wall application in our language},label={lst:live_wall}]
var posts = [{
  id: 0,
  msg: "Welcome to the Wall App!"
}]

def size = foreach (post in posts with y = 0) y+1

def post t = action {
  insert {id: size, msg: t} into posts
}
def deletePost id = action {
  delete post in posts where post.id == id
}

def page =
  <div class="container">
    <header>
      <h1>"Wall"</h1>
      <input type="text" name="text" placeholder="Post something"
             onenter=(post)/>
    </header>
    <ul>
    (map (p in posts)
      <li>
        <button class="delete" doaction=(deletePost p.id)>"X"</button>
        <span class="text">(p.msg)</span>
      </li>)
    </ul>
  </div>
\end{lstlisting}
In \cref{app:meteor_wall} we define the same application~\footnote{Working application at \url{https://meteor-wall.herokuapp.com/}} in Meteor.
The code is visibly a little more succinct in our prototype than in Meteor due to its file based nature and explicit division of client and server code.

Next, we want to transform the wall application into a multi-group application where users access a group wall and post anonymously.
In our language, we need only to wrap the previously defined application with an indexed module.
\Cref{lst:live_groups_wall} transforms the wall application in \cref{lst:live_wall} into a multi-group wall application~\footnote{Working example at \url{http://live-programming.herokuapp.com/app/aNRZd/Group/page/Family} for a family group wall. Change group by changing the last \gls{url} path (Family).}.
\begin{lstlisting}[caption={Multi-user wall application in our language},label={lst:live_groups_wall}]
module Group<string group> {
  // *@\cref{lst:live_wall}@* application code
}
\end{lstlisting}
In Meteor, to develop the same transformation we require the definition of routes, a new collection for the groups, and some other template helpers.
Additionally, in Meteor collections are available on both the client and the server, thus we need to specify what information is published to the client.
\Cref{app:meteor_groups_wall} transforms the example wall application in \cref{app:meteor_wall} into a multi-group wall application~\footnote{Working application at \url{https://wall-groups.herokuapp.com/group/Family} for a family group wall. Change group by changing the last \gls{url} path (Family)}.
The effort, as well as code written, to make the multi-tenant transformation is evidently greater with Meteor.
Furthermore, since the security rules and routes are all written by hand, there are no guarantees that they are error free.
Testing, as well as writing security rules and routes, require a greater development effort throughout the evolution of an application.
The transformation of an application to an \gls{mta} is visibly more simple and less prone to error in our language.

\section{TodoMVC Multi-Group}

In modern web frameworks, as an application adds layers of multi-tenant traits, the effort and possible errors in development grows.
The extended TodoMVC application~\footnote{Working example in the work space at \url{http://live-programming.herokuapp.com/dev/BluQ1}, with the authentication page at \url{http://live-programming.herokuapp.com/app/BluQ1/Public/page}} we implemented in \cref{ch:implementation_challenges} involves most of the multi-tenant traits described.
The code for our non styled and simplified TodoMVC for groups of users can be found in \cref{app:simple_todomvc}.
The code of our styled\footnote{The TodoMVC CSS can be found at.\url{https://github.com/tastejs/todomvc/blob/gh-pages/examples/backbone/node_modules/todomvc-app-css/index.css}} and complete TodoMVC application~\footnote{Working example at \url{http://live-programming.herokuapp.com/dev/lCBCd}, with the main login page at \url{http://live-programming.herokuapp.com/app/lCBCd/Public/page}} for groups of users can be found at \cref{app:todo_mvc_app}.
We built the same application in Meteor~\footnote{Working example at \url{https://todo-groups.herokuapp.com/} using the same users defined in \cref{app:users}}.
The code for the Meteor version can be found in \cref{app:todomvc_groups_meteor}.

Writing the application in Meteor requires slightly more written code, and a more careful plan to segment group information without any errors.
The simplest phase in the development of the application in Meteor is the use of the package manager to introduce a packaged authentication mechanism.
In our language, we also re-use a previously defined authentication mechanism but it is not packaged and distributed automatically, requiring a manual copy of the code to the new application.
Although it is quite simple to manually copy an application in our framework, it is important for the viability of any web framework to package and distribute applications.

Manually writing routes is a careful process as subscribing to required information needs to happen before the route delivers the desired result, or else some information might not be available on the client side when a piece of code requests data, which results in errors.
In our language, data is segmented naturally and automatically with each module definition, and each segmented data has a route of its own provided through a \gls{rest} interface.
The automatic segmentation of data is effortless and guarantees the safe development of applications, as opposed to the possibility of errors in the manual segmentation of data in Meteor and other similar modern web frameworks.

Even thou there is a package for Meteor to associate roles to authenticated users from the already used authentication package, the example only required one role for one action (delete to-dos) which proved simpler to just define a condition through a helper function.
Additionally, the package only creates roles for authenticated users, which leaves the creation and management of any other types of roles in any other application to be manual.
In our language, creating and managing roles is equally simple but also guarantees a sound application without broken security rules.
Further more roles are associated with defined nested modules which can represent anything, not just users.

\section{Conclusions}

Meteor produces very efficient applications performance wise, and although our proposed language can very rapidly transform applications into \glspl{mta}, its performance is noticeably slower due to many factors, some of which are discussed and listed as future work in \cref{ch:final_remarks}.
However, Meteor code is also written in \gls{js}, an untyped language that gives no guarantees of a sound application, which can increase the costs in the development phase when errors occur, moreover, in production there is always the possibility of errors occurring that were not caught in development.
In contrast, the core language in this thesis is typed and is as code succinct as Meteor.
Since our approach maps the introduced mechanisms to core language operations, the typing system of the language continues to provide the safe development of applications with guarantees that applications are sound.
Additionally, there is an increased cost in deployment for applications developed in modern frameworks, which in our framework is cost free with an automatic deployment, with changes to an application being applied on the running application as soon as they are verified.
